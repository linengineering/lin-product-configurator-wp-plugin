

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
ORDER OF OPERATIONS
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

- initiate main LPC class
- start PHP session
- include files
- register scripts 
    - main scripts and all configurator scripts
- load main shortcode
    - enqueue main scripts
- load main js files
- ajax call to get template
    - enqueue configurator scripts
    - if url string not set -> return category template
    - if url string is set:
        - generate product data and set to session
        - generate chart data and set to session
        - new session 
    - return configurator template

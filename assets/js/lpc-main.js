/**
 * 
 * Main Lin Product Configurator object
 *
 */

/* check if ie11 */
if(!!window.MSInputMethodContext && !!document.documentMode){
  alert('Rapid Prototype Configurator does not support Internet Explorer 11 Browser. We recommend using the latest version of Firefox, Chrome, or Edge Browser. We apologize for the inconvenience.');
}

$(document).foundation();

var lpc = new function() {

  /* Define LPC main constants */
  this.CATEGORY = 'c1';
  this.TYPE     = 'c2';
  this.STEP     = 's1';
  this.SUBSTEP  = 's2';
  this.TUTORIAL = 'j1';

  /**
   * available configurators - retrieved by AJAX call. 
   */
  this.configurators = {}

  this.foundation_apps = function(){

    var $sticky = new Foundation.Sticky($('#navbar-content'), {
      stickTo: 'bottom',
      marginBottom: 0,
      anchor: 'sticky-stop',
      btmAnchor: 'sticky-stop',
      stickyOn: 'small',
    });
    
    var $equalizer = new Foundation.Equalizer($('.main-area-container'), {
      equalizeByRow: true,
    });

    this.sticky_calc();
    this.equalize_calc();
  };

  this.reinit_sticky = function(){
    Foundation.reInit($('.sticky'));
  }

  this.reinit_equalizer = function(){
    /** wait 500 m/s for images to load properly or else it will calculate incorrectly */
    setTimeout(function(){
      Foundation.reInit($('.main-area-container'));
    });
  }

  this.sticky_calc = function(){
    if($('.sticky').length > 0){
      $('.sticky').foundation('_calc', true);
    }
  }

  this.equalize_calc = function(){
    if($('.main-area-container').length > 0){
      $('.main-area-container').foundation('getHeightsByRow', function(cb){
        $('.main-area-container').foundation('applyHeightByRow', cb);
      });
    }
  }

  /**
   * retrieve query string front the url and save it in json object
   * @return {object} returns JSON object 
   */
  this.get_query_string = function(){

    /* Get the query string from url */
    var string_from_url = location.search.substring(1);

    /* Convert query string into a JSON object if string is present */
    if(string_from_url.length > 0){
      query_string_json = JSON.parse('{"' + decodeURI(string_from_url).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
    } else {
      query_string_json = {};
    }
    return query_string_json;
  }

  /**
   * Request single parameter from query string.
   * @param {string} request 
   * @return {string}
   */
  this.param = function(request){
    var query_string = this.get_query_string();
    if(query_string.hasOwnProperty(request)){
      return query_string[request];
    } else {
      return '';
    }
  }

  /**
   * Update query string with new values and push new url to browser.
   */
  this.set_query_string = function(parameters = []){
    var query_string = this.get_query_string();
    $.each(parameters, function(query, value){
      query_string[query] = value;
    })
    var query_string = $.param(query_string);
    window.history.pushState('lpc', 'parameters', '?' + query_string);
  }

  /**
   * Check if query exists
   */
  this.is_query_set = function(query){
    var string = this.get_query_string();
    if(string.hasOwnProperty(query)){
      return true;
    } else {
      return false; 
    }
  }

  /**
   * Ajax friendly on click event shorthand
   * @param {string} c button or element class
   * @param {function} f function to pass
   */
  this.click = function(c, f){
    $(document).on('click', c, f);
  }

  /**
   * Reset specified values
   * @param {object} request 
   */
  this.reset = function(request){
    var reset = {};
    $.each(request, function(key, value){
        reset[value] = '';
    });
    this.set_query_string(reset);
}

  /**
   * show and hide loading spinner
   * @param {string} c parent class where spinner is located
   */
  this.show_spinner = function(c){
    $(c).find('#lpc-spinner').fadeIn();
  }
  this.hide_spinner = function(c){
    $(c).find('#lpc-spinner').fadeOut();
  }

} // END OF LPC MAIN CLASS
window.onscroll = function(){
  lpc.equalize_calc();
  lpc.sticky_calc();
}
window.onresize = function(){
  lpc.equalize_calc();
  lpc.sticky_calc();
}
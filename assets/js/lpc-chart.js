/**
 * Charting function
 */

var lpc_chart = new function(){

    this.chart; 

    this.chart_settings = {
        title: {
            text: 'title',
            fontColor: "#333",
            fontFamily: "arial",
            fontWeight: "bold",
            fontSize: 18,
        },
        subtitles: [
            {
                text: 'subtitle',
                fontColor: "#777",
                fontFamily: "arial",
                fontWeight: "lighter",
                fontSize: 14,
            }
        ],
        toolTip: {
            enabled: true
        },
        axisX: {
            title: "speed [in/sec]",
            titleFontSize: 12,
            fontFamily: "arial",
            viewportMinimum: 0,
        },
        axisY: {
            title: "force [lbs]",
            tickColor: "#eee",
            gridColor: "#eee",
            minimum: 0,
            titleFontSize: 12,
            fontFamily: "arial",
            viewportMinimum: 0,
        },
        exportEnabled: true,
        data: [],
        zoomEnabled: true, 
        zoomType: "xy"
    };

    this.set_chart = function(){
        this.chart = new CanvasJS.Chart("lpc-chart", this.chart_settings);
        this.chart.render();
    }

    this.replace_datasets = function(dataset){
        this.chart.options.data = dataset;
        this.chart.render();
    }

    /**
     * Set chart title
     * @param {string} title 
     */
    this.replace_title = function(title){
        this.chart.options.title.text = title;
        this.chart.render();
    }

    this.replace_subtitle = function(subtitle){
        this.chart.options.subtitles[0].text = subtitle;
        this.chart.render();
    }

}

var lpc_ajax = new function(){
  
  this.ajaxLoading = false; 

  /**
   * Chart data object
   */
  this.chart_data = {}; 

  /**
   * Main instance of ajax call used by LPC
   * @param {string} action 
   */
  this.ajax_post = function(action, post_items = {}){

    var query_string = lpc.get_query_string();

    var data = {
      'action': action,
      '_ajax_nonce': lpc_ajax_data.nonce,
    }

    /* Add custom parameters to data object */
    $.extend(data, post_items);

    /* Add parameters to data object */
    $.extend(data, query_string);

    return $.ajax({
        url: lpc_ajax_data.ajax_url + '?' + location.search.substring(1),
        type: 'POST',
        dataType: 'json',
        data: data,
    });
  };

  this.check_parameters = function(d){
    var string   = lpc.get_query_string();
    var validate = true;
    $.each(d, function(query, value){
      if(!string.hasOwnProperty(query)){
        validate = false;
        return false;
      }
    });
    return validate;
  };

  // get general template 
  this.get_template = function(){
    this.ajax_post('get_template').done(function(response){
      $('.configurator').html(response['template']).foundation();
      $('#lpc-preloader').fadeOut();
      lpc_chart.set_chart();
    });
  }

  // initiate main instance of configurator and get template
  this.configurator_setup = function(){
    this.ajax_post('configurator_setup').done(function(response){
      /**
       *  Request and set parameters. Check if parameters from server exist, and that parameters are not already loaded. 
       *  If parameters already exist in query string and they're valid, use them. 
      */
      if(response['parameters'] != null && lpc_ajax.check_parameters(response['parameters']) == false){
        lpc.set_query_string(response['parameters']); 
      }

      if(response['chart_data'] != null){
        lpc_ajax.chart_data = response['chart_data'];
      }
  
      // get and load template
      lpc_ajax.ajax_post('get_template').done(function(response){
        $('.configurator').html(response['template']).foundation();
        $('#lpc-preloader').fadeOut();
        
        // Set and configure chart
        lpc_chart.set_chart();

        var category = lpc.param(lpc.CATEGORY); 
        var type = lpc.param(lpc.TYPE);

        /* load initial chart data */
        eval('lpc_'+category+'_'+type+'_chart').load();

        /** initiate foundation plugins */
        lpc.foundation_apps();
      });

    });
  }

  this.get_chart_data = function(){
    this.ajax_post('get_chart_data').done(function(response){
      lpc_ajax.chart_data = response;
    });
  }

} // END OF LPC AJAX MAIN CLASS

$(window).on('load', function(){
  lpc_ajax.configurator_setup();
}); 

lpc.click('.select-category', function(){
  var id = this.id;
  lpc.set_query_string({c1: id});
  lpc_ajax.configurator_setup();
});

lpc.click('.select-type', function(){
  var id = this.id;
  lpc.set_query_string({c2: id});
  window.location.reload(true);
});
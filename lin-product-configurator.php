<?php
/**
 * Plugin Name: Lin Product Configurator
 * Description: Product Configurator tool for quick prototyping and purchasing of Lin Engineering Products.
 * Version: 1.0.0
 * Author: Bart Pichola
 *
 * @package LPC
 *
**/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/* check if woocommerce is active */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	// Define LPC_PLUGIN_FILE.
	if ( ! defined( 'LPC_PLUGIN_FILE' ) ) {
		define( 'LPC_PLUGIN_FILE', __FILE__ );
	}

	// Include the main LPC class.
	if ( ! class_exists( 'LPC' ) ) {
		include_once dirname( __FILE__ ) . '/includes/class-lpc.php';
	}

	// load main instance of LPC after woocommerce loaded
	add_action('woocommerce_loaded', function(){

		/* Main instance of LPC. */
		LPC::instance();
		
	});
}

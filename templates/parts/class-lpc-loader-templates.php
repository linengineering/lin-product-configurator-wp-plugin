<?php
/**
 * Preloaders and spinners
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_loader_templates {

	public function loader_spinner(){
		ob_start();
		?>
		<div class="lpc-loader-outer" id="lpc-spinner">
			<div class="lpc-loader-inner">
				<i class="fas fa-spinner fa-spin fa-5x"></i>
			</div>
		</div>
		<?php
		return ob_get_clean();
	}

	public function configurator_preloader(){
		ob_start();
		?>
		<div class="lpc-preloader-outer" id="lpc-preloader">
			<div class="lpc-preloader-inner">
				<i class="fas fa-spinner fa-spin fa-5x"></i>
				<p>Loading Rapid Prototype Configurator. Please Wait...</p>
			</div>
		</div>
		<?php
		return ob_get_clean();
	}
	
}
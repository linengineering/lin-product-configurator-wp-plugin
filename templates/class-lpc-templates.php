<?php
/**
 * Main Template class
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_templates {

	/**
	 * Configurator object
	 * @var object
	 */
	public $configurator;

	public function __construct(){
		$this->configurator = new LPC_configurator_data;
		$this->navigation	= new LPC_navigation_handler;
		$this->category     = $this->configurator->current_validated_category;
		$this->type         = $this->configurator->current_validated_type;
		$this->step         = param(STEP);
		$this->substep      = param(SUBSTEP);
	}

	public function get_template(){

		if($this->category == null && $this->type == null){
			return $this->product_category_page_template();
		} else if ($this->category != null && $this->type == null){
			return $this->product_type_page_template();
		} else if ($this->category != null && $this->type != null){
			return $this->configurator_main_template();
		}

	}

	public function main_configurator_container(){
		$loaders = new LPC_loader_templates;
		ob_start();
		?>
			<main class="configurator">
				<?php echo $loaders->configurator_preloader(); ?>
			</main>
		<?php 
		return ob_get_clean();
	}

	public function product_category_page_template(){

		$category_array = array();

		foreach($this->configurator->all_configurators->configurators as $key => $value){

			if(!in_array($value['category'], $category_array)){
				array_push($category_array, $value['category']);
			}

		}
		ob_start(); ?>
		<p>Choose configurator category:</p>
		<?php foreach($category_array as $value):?>
			<button class="button select-category large" id="<?php echo $value ?>"><?php echo $value ?></button>
		<?php endforeach;
		return ob_get_clean();
	}

	public function product_type_page_template(){

		$type_array = array();

		foreach($this->configurator->all_configurators->configurators as $key => $value){
			if($value['category'] == $this->category && !in_array($value['type'], $type_array)){
				array_push($type_array, $value['type']);
			}
		}
		ob_start(); ?>
		<p>Choose configurator type:</p>
		<?php foreach($type_array as $value): ?>
			<button class="button select-type large" id="<?php echo $value ?>"><?php echo $value ?></button>
		<?php endforeach;
		return ob_get_clean();
	}

	public function configurator_main_template(){
		$loaders = new LPC_loader_templates;
		ob_start(); 
			echo $loaders->configurator_preloader();
			if($this->category == null && $this->type == null){
				echo '<p>Configurator cannot be found, category or type does not match the record.</p>';
			} else {
				echo $this->configurator_template();
			}
		return ob_get_clean();
	}

	public function configurator_template(){
		$loaders = new LPC_loader_templates;
		ob_start(); 
		?>
		<div class="main-and-sidebar-container">

			<div class="main-area">
				<?php echo $loaders->loader_spinner(); ?>
				<div class="main-area-container" id="main-content">
					<?php echo $this->main_content(); ?>
				</div>
			</div>

			<div class="side-area">
				<?php echo $loaders->loader_spinner(); ?>
				<div class="side-area-container" id="sidebar-content" tooltipPosition="left" data-scrollTo='tooltip' data-step="3" data-intro="Choose the best product or configuration that fits your requirements">
					<?php echo $this->sidebar_content(); ?>
				</div>					
			</div>

		</div>

		<div class="nav-container">
			<nav id='navbar-content'>
				<?php echo $this->navbar_content(); ?>
			</nav>
		</div>

		<?php 
			echo $this->message_box();
			echo $this->tutorial();
		?>

		<div id='sticky-stop'></div>

		<?php
		return ob_get_clean();

	}

	public function main_content(){

		$current_step_array = $this->configurator->current_step_data;
		$current_substep_array = $this->configurator->current_substep_data;

		ob_start(); 
		?>
		<header class="main-header">
			<span class="configurator-step"><?php echo $current_step_array['step']; ?></span>
			<span class="configurator-label" data-tooltip data-position="bottom" data-alignment="left" tabindex="1" title="<?php echo $current_step_array['desc']; ?>"><?php echo $current_step_array['name'] ?></span>
			<span class="configurator-descriptor">\</span>
			<span class="configurator-descriptor" data-tooltip data-position="bottom" data-alignment="left" tabindex="1" title="<?php echo $current_substep_array['desc']; ?>"><?php echo $current_substep_array['name'] ?></span>
			<span class="configurator-label right" data-tooltip data-position="bottom" data-alignment="left" tabindex="1" title="Learn how to use the Rapid Prototype Configurator" ><a data-open="tutorial-box"><i class="fas fa-mouse-pointer"></i> Tutorial</a></span>
		</header>
		<?php

		do_action('lpc_main_area_content_' . $this->category . '_' . $this->type );

		return ob_get_clean();
	}

	public function sidebar_content(){
		ob_start(); 
		?>
		
		<section class="totals">
			<span class='total-price'>Total Price:<span class='total-price-value'><?php printf('$%.2f', $this->configurator->configurator->get_price()); ?></span></span>
			<span class='total-lead-time'>Est. Lead Time:
				<span class='total-lead-time-value'><?php printf( '%d Days', $this->configurator->configurator->get_lead_time()); ?></span>
				<span data-tooltip tabindex="1" data-position="bottom" data-alignment="left" title="Estimated prototype build time in business days. Not including shipping time."><i class="fas fa-question-circle"></i></span>
			</span>
		</section>

		<?php
		do_action('lpc_side_bar_content_' . $this->category . '_' . $this->type );
		
		return ob_get_clean();
	}

	public function navbar_content(){

		if(!$this->configurator->configurator == null){
			
			$completed_steps = $this->configurator->configurator->completed_steps();
			$next_step       = $this->navigation->navigate_steps('next');
			$previous_step   = $this->navigation->navigate_steps('previous');

			ob_start();
			?>
			<button class="step-button enabled" data-step="<?php echo $previous_step['step']; ?>" data-substep="<?php echo $previous_step['substep']; ?>"><i class="fas fa-caret-left"></i></button>
			<?php foreach($this->configurator->configurator->steps as $step => $step_data): ?>
				<?php 
				if(isset($completed_steps[$step])){
					
					if(param(STEP) == $step && in_array(false, $completed_steps[$step])){
						$progess_class = 'active';
					}
					else if(in_array(true, $completed_steps[$step]) && in_array(false, $completed_steps[$step])){
						$progess_class = 'active';
					}
					else if(in_array(true, $completed_steps[$step]) && !in_array(false, $completed_steps[$step])) {
						$progess_class = 'complete';
					}
					else if(!in_array(true, $completed_steps[$step]) && in_array(false, $completed_steps[$step])) {
						$progess_class = '';
					}

					$all_steps_completed = true;
					foreach($completed_steps as $key => $arr){
						if($key != 'review'){
							if(in_array(false, $arr)){
								$all_steps_completed = false;
							}
						}
					}

					/** if all steps are completd, mark review tab as complete */
					if($step == "review" && $all_steps_completed){
						$progess_class = 'complete';
					}

				} else {
					$progess_class = '';
				}

				?>
				<div class="nav-step-container <?php echo $progess_class; ?>">
					<div class="step <?php echo $progess_class; ?>" data-step="<?php echo $step; ?>" data-substep="<?php echo key($step_data['substeps']); ?>">
						<?php echo $this->circle_number($step_data['step']);?>
						<span class="step-text-lable"><?php echo $step_data['name']; ?></span>
						<?php if($progess_class == 'complete'){ echo '<i class="fas fa-check"></i>'; }; ?>
						<?php if($progess_class == 'active'){ echo '<i class="fas fa-cog fa-spin"></i>'; }; ?>
					</div>
				</div>
				<?php if($this->step == $step): ?>
					<div class="nav-substep-container">
					<?php foreach($step_data['substeps'] as $substep => $substep_data): ?>
						<div class="substep <?php if($this->substep == $substep){echo 'active';} ?> <?php if($completed_steps[$step][$substep]){ echo 'complete'; }; ?>" data-step="<?php echo $step; ?>" data-substep="<?php echo $substep; ?>">
							<span class="substep-text-lable"><?php echo $substep_data['name']; ?></span>
							<?php if($completed_steps[$step][$substep]){ echo '<i class="fas fa-check"></i>'; }; ?>
						</div>
					<?php endforeach; ?>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
			<?php 
				$next_enable = ''; 
				if(isset($completed_steps[$this->step][$this->substep])){;
					if($completed_steps[$this->step][$this->substep]){ 
						$next_enable = 'enabled glow'; 
					}
				}
			?>
			<button class="step-button wide <?php echo $next_enable; ?>" data-step="<?php echo $next_step['step']; ?>" data-substep="<?php echo $next_step['substep']; ?>">Next <i class="fas fa-caret-right"></i></button>
			<?php 
			return ob_get_clean();
		} else {
			return 'data not found';
		}
	}

	public function circle_number($num){
		ob_start();
		?>
			<span class="fa-layers fa-fw">
				<i class="fas fa-circle" data-fa-transform="grow-7"></i>
				<span class="fa-layers-text fa-inverse" data-fa-transform="up-1"><?php echo $num; ?></span>
			</span>
		<?php 
		return ob_get_clean();
	}

	public function message_box(){
		ob_start();
			?>
			<div class="reveal" id="message-box" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">    
				<div id="message-content"></div>
				<button class="close-button" data-close aria-label="Close modal" type="button">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?php 
		return ob_get_clean();
	}

	public function tutorial(){
		ob_start();
			?>
			<div class="reveal" id="tutorial-box" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">    
				<div id="message-content">
					<h3>Prototype Fast!</h3>
					<p>Rapid Prototype Configurator makes it easy to configure and order prototypes fast. <strong>Would you like to learn how to use it?</strong></p>
					<a class="button large expanded" href="javascript:void(0);" onclick="javascript:introJs().start();" data-close aria-label="Close modal">Show Me How To Use It!</a>
				</div>
				<button class="close-button" data-close aria-label="Close modal" type="button">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?php 
		return ob_get_clean();
	}
}

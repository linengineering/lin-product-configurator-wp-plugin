<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_parameters {

  /**
   * Fetch POSTED values passed via AJAX. 
   * @param  string $request name of the requested parameter
   * @return string          requested value from the current query string
   */
  public function get_parameter_from_post($request){
    if(isset($_POST[$request])){
      return filter_var($_POST[$request], FILTER_SANITIZE_STRING);
    } else {
      return null;
    }
  }

  public function get_parameter_from_get($request){
    if(isset($_GET[$request])){
      return filter_var($_GET[$request], FILTER_SANITIZE_STRING);
    } else {
      return null;
    }
  }

  public function parameter($request){
    if(isset($_POST[$request])){
        return $this->get_parameter_from_post($request);
    } else {
        return $this->get_parameter_from_get($request);
    }
  }

}

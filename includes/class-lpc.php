<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * LPC setup
 *
 * @package  PLC
 * @since    1.0.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Main LPC Class.
 *
 * @class Lin_product_configurator
 */
final class LPC{

  /**
	 * The single instance of the class.
	 *
	 * @var LPC
	 * @since 1.0.0
	 */
	protected static $_instance = null;

  /**
   * Main LPC Instance.
   *
   * Ensures only one instance of LPC is loaded or can be loaded.
   *
   * @since 1.0.0
   * @static
   * @see LPC()
   * @return LPC - Main instance.
   */
  public static function instance() {
    if ( is_null( self::$_instance ) ) {
      self::$_instance = new self();
    }
    return self::$_instance;
  }

    /**
	 * LPC Constructor.
	 */
	public function __construct() {
		$this->define_constants();
		$this->includes();
		$this->hooks();
	}

	public function hooks(){
		add_action('init', array($this, 'start_session'), 1);
		add_shortcode('LIN_PRODUCT_CONFIGURATOR', array($this, 'configurator_shortcode'));
	}

   /**
	 * Define all LPC plugin constants.
	 */
  	public function define_constants(){
		/* plugin directory */
		define( 'LPC_ABSPATH', dirname( LPC_PLUGIN_FILE ) . '/' );
		
		/* Category Handlers */
		if(!defined('CATEGORY')){
			define('CATEGORY', 'c1');
			define('TYPE', 'c2');
			define('STEP', 's1');
			define('SUBSTEP', 's2');			
		}
	 }
	 
	/**
	 * Initiate user session if session does not exist
	 *
	 * @return void
	 */
	public function start_session(){
		if(!session_id()) {
			session_start();
		}
	}  

    /**
	 * File includes.
	 */
  	public function includes(){

		/* Class Autoloader */
		//include_once LPC_ABSPATH . 'includes/class-lpc-autoloader.php';

		/** Enqueue or register scripts */
		include_once LPC_ABSPATH . 'includes/class-lpc-enqueue-scripts.php';

		/* Funtions */
		include_once LPC_ABSPATH . 'includes/lpc-parameter-request.php';

		// Classes
		include_once LPC_ABSPATH . 'includes/class-lpc-ajax-handler.php';
		include_once LPC_ABSPATH . 'includes/class-lpc-parameters.php';
		
		include_once LPC_ABSPATH . 'includes/class-lpc-products.php';
		include_once LPC_ABSPATH . 'includes/class-lpc-colors.php';

		// Configurators
		include_once LPC_ABSPATH . 'configurators/class-lpc-all-configurators-setup.php';
		include_once LPC_ABSPATH . 'configurators/class-lpc-configurator-data.php';

		// Templates
		include_once LPC_ABSPATH . 'templates/parts/class-lpc-loader-templates.php';
		include_once LPC_ABSPATH . 'templates/class-lpc-templates.php';

		// Base ajax events
		include_once LPC_ABSPATH . 'includes/class-lpc-ajax-events.php';

		// Navigation Handler 
		include_once LPC_ABSPATH . 'includes/class-lpc-navigation-handler.php';

		// Shopping Cart
		include_once LPC_ABSPATH . 'includes/class-lpc-check-out.php';
  	}

	/**
	 * LPC main shortcode
	 *
	 * @return void
	 */
	public function configurator_shortcode(){

		$category = param(CATEGORY);
		$type 	  = param(TYPE);

		/**
		 * Enqueue registered scripts when shortcode is loaded
		 */
		$scripts = new LPC_enqueue_scripts;
		$scripts->scripts_to_enqueue();

		/**
		 * Load main configurator container
		 */
		$template = new LPC_templates();
		echo $template->main_configurator_container();
	}

}

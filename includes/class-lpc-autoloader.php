<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Autoloads classes using WordPress naming convention (class_name_of_class).
 */
class LPC_Autoloader
{
    /**
     * Registers LPC_Autoloader as an SPL autoloader.
     */
    public static function register($prepend = false)
    {
        if (version_compare(phpversion(), '5.3.0', '>=')) {
            spl_autoload_register(array(new self, 'autoload'), true, $prepend);
        } else {
            spl_autoload_register(array(new self, 'autoload'));
        }
    }

    public static function autoload($class)
    {
        if (0 !== strpos($class, 'LPC')) {
            return;
        }

        if (is_file($file = dirname(__FILE__).'/class-'.strtolower(str_replace(array('_', "\0"), array('-', ''), $class).'.php'))) {
            require_once $file;
        }
    }
}

new LPC_Autoloader;

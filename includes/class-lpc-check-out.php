<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_check_out {

    public function __construct(){
        $this->actions();
    }

    function actions(){
        add_filter( 'woocommerce_get_item_data', array($this, 'get_item_data'), 10, 2 );
        add_action( 'woocommerce_before_calculate_totals', array($this, 'add_custom_price'), 99 );
        add_action( 'woocommerce_checkout_create_order_line_item', array($this, 'add_meta_to_order_items'), 20, 4 );
        add_filter( 'woocommerce_email_order_meta_fields', array($this, 'email_order_meta_fields'), 20, 3 );
        add_filter( 'woocommerce_cart_item_permalink', array($this, 'cart_item_permalink'), 10, 3);
        add_filter( 'woocommerce_order_item_permalink', array($this, 'order_item_permalink'), 10, 3);
    }

    public function get_item_data($item_data, $cart_item){
        foreach($cart_item as $key => $item){
            $key_id = substr($key, 0, 3);
            if($key_id == 'lpc'){
                $title = ucwords(str_replace(['_', 'lpc'], ' ', $key));
                $item_data[] = array(
                    'name'  => $title,
                    'value' => $item,
                );                
            }
        }
        return $item_data;
    }

    public function add_custom_price($cart_object){
        foreach($cart_object->cart_contents as $cart_item){
            if(isset($cart_item['config_price'])){
                $product_id   = $cart_item['product_id'];
                $custom_price = $cart_item['config_price'];
                $cart_item['data']->set_price($custom_price);
            }
        }
    }

    public function add_meta_to_order_items( $item, $cart_item_key, $values, $order ) {

        foreach($values as $key => $value){
            $key_id = substr($key, 0, 3);
            if($key_id == 'lpc'){
                $title = (string)ucwords(str_replace(['_', 'lpc'], ' ', $key));
                $item->add_meta_data($title, $value);
            }
        }
    } 

    public function email_order_meta_fields( $fields, $sent_to_admin, $order ) {
        $order_email_meta = $_SESSION['order_email_meta'];
        foreach($order_email_meta as $key => $value){
            $fields[$key] = array(
                'label' => $key,
                'value' => $value,
            );
        }
        $sent_to_admin = true;
        return $fields;
    }

    public function cart_item_permalink( $product_get_permalink_cart_item, $cart_item, $cart_item_key ){
        if(isset($_SESSION['item_permalink'])){
            $product_get_permalink_cart_item = $_SESSION['item_permalink'];
        } else {
            $product_get_permalink_cart_item = '';
        }
        return $product_get_permalink_cart_item;
    }    

    public function order_item_permalink( $product_get_permalink_item, $item, $order ){
        if(isset($_SESSION['item_permalink'])){
            $product_get_permalink_item = $_SESSION['item_permalink'];
        } else {
            $product_get_permalink_item = '';
        }
        return $product_get_permalink_item;
    }
}

new LPC_check_out; 
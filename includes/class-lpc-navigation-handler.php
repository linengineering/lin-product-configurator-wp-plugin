<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_navigation_handler {

    public function __construct(){
        $this->configurator = new LPC_configurator_data();
    }
    /**
     * Returns next or previous configuration step. 
     *
     * @param string $direction - idicate direction "next" or "previous"
     * @return array returns array with next step and substep
     */
    public function navigate_steps($direction){

        $flat_steps        = $this->configurator->flat_steps();
        $current_step      = param(STEP);
        $current_substep   = param(SUBSTEP);
        $current_position  = null;

        if(!$current_step == null){

            foreach($flat_steps as $key => $value){
                if($value['step'] == $current_step && $value['substep'] == $current_substep){
                    $current_position = $key;
                }
            }
    
            $position = ($direction == 'next') ? $current_position + 1 : $current_position - 1;
            
            if(isset($flat_steps[$position])){
                return $flat_steps[$position];
            } else {
                return $flat_steps[$current_position];
            }

        } else {
            return array('step' => false, 'substep' => false);
        }
    }
}
<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Main AJAX handler for LPC
 */
class LPC_ajax_handler {

  /**
   * Action hook used by the AJAX class.
   *
   * @var string
   */
  const ACTION = 'lpc';

  /**
   * Action argument used by the nonce validating the AJAX request.
   *
   * @var string
   */
  const NONCE = 'lpc-ajax';

  /**
   * Register the AJAX handler class with all the appropriate WordPress hooks.
   */
  public $ajax_events = array();

  public function register_events(){
    $ajax_events = $this->ajax_events;
    foreach($ajax_events as $ajax_event => $nopriv){
      add_action('wp_ajax_' . $ajax_event, array($this, $ajax_event));
      if($nopriv){
        add_action('wp_ajax_nopriv_' . $ajax_event, array($this, $ajax_event));
      }
    }
  }

  public function localize_scripts(){
    add_action('wp_enqueue_scripts', array($this, 'scripts_to_localize'));
  }

  /**
   * Register our AJAX JavaScript.
   */
  public function scripts_to_localize(){
    // Placeholder for class extensions
  }

  /**
   * Get the AJAX data that WordPress needs to output.
   *
   * @return array
   */
  protected function get_ajax_data(){
      return array(
          'ajax_url'    => admin_url( 'admin-ajax.php' ),
          'nonce'       => wp_create_nonce(self::NONCE),
      );
  }
}

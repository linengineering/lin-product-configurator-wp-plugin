<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * Make it easier to manager Parameter requests;
 */
function param($request){
    $parameters = new LPC_parameters; 
    return $parameters->parameter($request);
}
<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Handles common configurator ajax request
 */
class LPC_ajax_events extends LPC_ajax_handler {

    public $config_array;
    public $configurator;
    public $parameters; 

    /**
     * Define AJAX events [ajax event function] => [nopriv]
     * @var array
     */
    public $ajax_events = array(
      'post_parameters'      => true,
      'configurator_setup'   => true,
      'get_template'         => true, 
      'get_main'             => true,
      'get_side'             => true,
      'get_nav'              => true,
    );

    static function init(){

      $handler = new self;
      /* initiate parameter handler class */
      $handler->parameters = new LPC_parameters();

      /* registers wp_ajax and wp_ajax_nopriv events from ajax_events array */
      $handler->register_events();
      $handler->localize_scripts();
    }

    public function scripts_to_localize(){
      wp_localize_script('lpc-ajax', 'lpc_ajax_data', $this->get_ajax_data());
    }

    /**
     * Return specific elements methods
     */
    public function element_main(){
      $templates = new LPC_templates;
      return $templates->main_content();
    }

    public function element_side(){
      $templates = new LPC_templates;
      return $templates->sidebar_content();
    }

    public function element_nav(){
      $templates = new LPC_templates;
      return $templates->navbar_content();
    }

    public function element_template(){
      $template_data = new LPC_templates;
      return $template_data->get_template();      
    }

    public function element_chart_data(){
      $config_data  = new LPC_configurator_data;
      /* Generate product and chart data SESSION */
      if(isset($config_data->configurator)){
        return $config_data->configurator->get_chart_data_session();
      } else {
        return null;
      }
    }


    /**
     * Ajax Requests
     */
    public function post_parameters(){
      check_ajax_referer(self::NONCE);
      wp_send_json('POST success');
      die();
    }

    /**
     * AJAX call to generate new product and chart data and set as $_SESSION object. 
     * Return defaults parameters of this specific configuration. 
     * @return object json
     */
    public function configurator_setup(){
      check_ajax_referer(self::NONCE);

      $config_data  = new LPC_configurator_data;

      /* Generate product and chart data SESSION */
      if(isset($config_data->configurator)){
        $config_data->configurator->set_products_data_session();
        $config_data->configurator->set_chart_data_session();
        $parameters   = $config_data->configurator->parameters;
      } else {
        $parameters = null;
      }

      wp_send_json(['parameters' => $parameters, 'chart_data' => $this->element_chart_data()]);
      die();
    }

    public function get_chart_data(){
      check_ajax_referer(self::NONCE);
      wp_send_json(['chart_data' => $this->element_chart_data()]);
      die();
    }

    public function get_template(){
      check_ajax_referer(self::NONCE);
      wp_send_json(['template' => $this->element_template()]);
      die();
    }

    public function get_main(){
      check_ajax_referer(self::NONCE);
      wp_send_json($this->element_main());
      die();
    }

    public function get_side(){
      check_ajax_referer(self::NONCE);
      wp_send_json($this->element_side());
      die();
    }

    public function get_nav(){
      check_ajax_referer(self::NONCE);
      wp_send_json($this->element_nav());
      die();
    }

}
LPC_ajax_events::init();

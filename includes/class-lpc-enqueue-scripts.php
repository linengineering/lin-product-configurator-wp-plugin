<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * This class handles all main script and styles
 */
class LPC_enqueue_scripts {

    static function register_scripts(){
        add_action('wp_enqueue_scripts', array(new self, 'scripts_to_register'));
    }

    public function scripts_to_register(){
        /** Canvas JS for generating charts */
        wp_register_script( 'lpc-canvas-js', plugin_dir_url( LPC_PLUGIN_FILE ) . 'assets/js/jquery.canvasjs.min.js', array('jquery'), '', false );

        /** Main javascript script for LPC configurator */
		wp_register_script( 'lpc-main', plugin_dir_url( LPC_PLUGIN_FILE ) . 'assets/js/lpc-main.js', array('jquery', 'foundation'), '', true );
    
        /** Main AJAX script for LPC configurator */
        wp_register_script('lpc-ajax', plugin_dir_url( LPC_PLUGIN_FILE ) . 'assets/js/lpc-ajax.js', array('lpc-main'), '', true);

        /** Main Chart script for LPC configurator */
        wp_register_script('lpc-chart', plugin_dir_url( LPC_PLUGIN_FILE ) . 'assets/js/lpc-chart.js', array('lpc-ajax'), '', true);

		wp_register_script( 'lpc-intro-js', 'https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/intro.min.js', array('jquery'), '', true );
		wp_register_style( 'lpc-intor-css', 'https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/introjs.min.css' );
    }

    static function enqueue_scripts(){
        add_action('wp_enqueue_scripts', array(new self, 'scripts_to_enqueue'));
    }

    public function scripts_to_enqueue(){

        wp_enqueue_script( 'lpc-canvas-js' );
		wp_enqueue_script( 'lpc-main' );
        wp_enqueue_script( 'lpc-ajax' );
        wp_enqueue_script( 'lpc-chart' );
		wp_enqueue_script( 'lpc-intro-js' );
        wp_enqueue_style( 'lpc-intor-css' );
        
    }

}

/** Register scripts on initiate */
LPC_enqueue_scripts::register_scripts();
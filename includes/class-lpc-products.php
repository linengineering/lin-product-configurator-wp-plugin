<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


class LPC_products {

  public $default_category;

  public function __construct($default_category = null){
    $this->default_category = $default_category;
  }

  public function get_product_category_data(){
    
    $default_cat = get_term_by('slug', $this->default_category, 'product_cat');

    $default_cat_id = $default_cat->term_id;
    
    $product_category_data = get_terms('product_cat', array(
      'taxonomy'  => 'product_cat',
      'parent'    => $default_cat_id,
    ));
    
    return $product_category_data;
  }

  public function get_series_data(){

    /* data array */
    $data = array(); 

    $series_cat = $this->get_product_category_data();
    foreach($series_cat as $series){
      
      $data[$series->term_id]['series_name']      = $series->name;
      $data[$series->term_id]['count']            = $series->count;
      $data[$series->term_id]['parent']           = $series->parent;
      $data[$series->term_id]['term_id']          = $series->term_id;
      $data[$series->term_id]['term_taxonomy_id'] = $series->term_taxonomy_id;

      $category_meta = get_term_meta($series->term_id);
      $data[$series->term_id]['thumbnail_id']     = (int)$category_meta['thumbnail_id'][0];

      /* get product IDs associated with this category */
      $products = wc_get_products(['category' => [$series->name]]);
      foreach($products as $product){
        $terms = get_post_meta($product->get_id());
        $data[$series->term_id]['products'][$product->get_id()]['name'] = $product->get_name(); 
        $data[$series->term_id]['products'][$product->get_id()]['term_id'] = $product->get_id(); 
      }
    }
    return $data;
  }
}

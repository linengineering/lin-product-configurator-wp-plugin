<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * This class is responsible for gathering and validating data from the configurator object
 */
class LPC_configurator_data {

  /**
   * Parameters object
   * @var object
   */
  public $parameters;

  /**
   * configurator object
   * @var object
   */
  public $configurator;

  /**
   * all configurators object
   * @var object
   */
  public $all_configurators;

  /**
   * Stores values of current step for easy access
   * @var array
   */
  public $current_step_data;

  /**
   * Stores values of current step for easy access
   * @var array
   */
  public $current_substep_data;

  /**
   * Stores validated category.
   * @var string
   */
  public $current_validated_category = null;

  /**
   * Stores validated type.
   * @var string
   */
  public $current_validated_type = null;

  /**
   * Stores validated type.
   * @var array
   */
  public $flat_steps;


  public function __construct(){
    $this->parameters = new LPC_parameters();
    $this->all_configurators = new LPC_all_configurators_setup();
    $this->get_configurator_data();
    $this->get_current_step_data();
    $this->get_current_substep_data();
    $this->get_current_validated_category();
    $this->get_current_validated_type();
  }

  /**
   * Get default data from configurator and set to $configurator array
   * @return array $configurator
   */
  public function get_configurator_data(){
    $category_from_uri = param(CATEGORY);
    $type_from_uri = param(TYPE);

    /**
     * Name of the configurator setup class with namespace: \[category]\[subcategory]\[class_name]
     * @var string
     */
    $class = '\\' . $category_from_uri . '\\' . $type_from_uri . '\\LPC_configurator_setup';

    /**
     * Instantuate class if it exists. Null if false
     * @var object
     */
    if(class_exists($class)){
      $this->configurator = new $class();
    } else {
      $this->configurator = null;
    }
  }

  /**
   * Get current step data from the configurator object
   * @return array current_step_data
   */
  public function get_current_step_data(){

    $step_from_uri = param(STEP);

    if(!$step_from_uri == null && !$this->configurator == null){
      foreach($this->configurator->steps as $step => $data){
        if($step == $step_from_uri){
          $this->current_step_data = $data;
        }
      }
    } else {
      $this->current_step_data = null;
    }
  }

  /**
   * Get current substep data from the configurator object
   * @return array current_substep_data
   */
  public function get_current_substep_data(){

    $substep_from_uri = param(SUBSTEP);

    if(!$this->current_step_data == null && !$this->configurator == null){

      $substep_array = $this->current_step_data['substeps'];

      foreach($substep_array as $substep => $data){
        if($substep == $substep_from_uri){
          $this->current_substep_data = $data;
        }
      }
    } else {
      $this->current_substep_data = null;
    }
  }

  /**
   * Get current category and validate it against configurator parameters
   * @return array $current_validated_category
   */
   public function get_current_validated_category(){

     $all_configurators_array = $this->all_configurators->configurators;
     $category_from_uri = param(CATEGORY);

     foreach($all_configurators_array as $key => $value){
       if($value['category'] == $category_from_uri){
         $this->current_validated_category = $value['category'];
       }
     }

   }

   /**
    * Get current type and validate it against configurator parameters
    * @return array $current_validated_type
    */
    public function get_current_validated_type(){

      $all_configurators_array = $this->all_configurators->configurators;
      $type_from_uri = param(TYPE);

      foreach($all_configurators_array as $key => $value){
        if($value['type'] == $type_from_uri){
          $this->current_validated_type = $value['type'];
        }
      }
    }

    public function flat_steps(){
      $flat_steps = array();
      foreach($this->configurator->steps as $step_name => $step_data){
        foreach($step_data['substeps'] as $substep_name => $substep_data){
          $push = [
            'step' => $step_name,
            'substep' => $substep_name,
          ];
          array_push($flat_steps, $push);
        }
      }
      return $flat_steps;
    }
}

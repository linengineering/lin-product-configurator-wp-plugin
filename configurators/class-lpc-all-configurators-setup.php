<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_all_configurators_setup {

  /**
   * Declare available configurators using [type] => [category];
   * @var array
   */
  public $configurators = array(
    'linear-le' => array(
      'category'    => 'linear',
      'type'        => 'le',
    )
  );

  public function __construct(){
    $this->include_configurators();
  }

  /**
   * include configurator setup file for each configurator
   */
  public function include_configurators(){
    foreach($this->configurators as $key => $value){
      $file = LPC_ABSPATH . 'configurators/' . $value['category'] . '/' . $value['type'] . '/class-lpc-configurator-setup.php';
      if(file_exists($file)){
        include_once $file;
      }
    }
  }
}
new LPC_all_configurators_setup;
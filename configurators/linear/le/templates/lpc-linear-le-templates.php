<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * This class controls templates of linear le products for all steps 
 */

class LPC_linear_le_templates {

    public function __construct(){
        $this->parameters = new \LPC_parameters();
        $this->step = $this->parameters->parameter(STEP);
        $this->substep = $this->parameters->parameter(SUBSTEP);
        $this->actions();
    }

    public function actions(){
        add_action('lpc_main_area_content_linear_le', array($this, 'main_area'));
        add_action('lpc_side_bar_content_linear_le', array($this, 'side_bar'));
    }

    public function main_area(){

        if($this->step == 'motor' && $this->substep == 'series'){
            $a = new Lpc_linear_le_step_motor_series_template();
            $a->main_area_template();
        }
        if($this->step == 'motor' && $this->substep == 'length'){
            $a = new Lpc_linear_le_step_motor_length_template();
            $a->main_area_template();
        }
        if($this->step == 'leadscrew' && $this->substep == 'options'){
            $a = new LPC_linear_le_step_leadscrew_options_template();
            $a->main_area_template();
        }
        if($this->step == 'leadscrew' && $this->substep == 'modifications'){
            $a = new LPC_linear_le_step_leadscrew_modifications_template();
            $a->main_area_template();
        }
        if($this->step == 'nut' && $this->substep == 'options'){
            $a = new LPC_linear_le_step_nut_options_template();
            $a->main_area_template();
        }
        if($this->step == 'review' && $this->substep == 'configuration'){
            $a = new LPC_linear_le_step_review_template();
            $a->main_area_template();
        }
        
    }

    public function side_bar(){

        if($this->step == 'motor' && $this->substep == 'series'){
            $a = new Lpc_linear_le_step_motor_series_template();
            $a->side_bar_template();
        }
        if($this->step == 'motor' && $this->substep == 'length'){
            $a = new Lpc_linear_le_step_motor_length_template();
            $a->side_bar_template();
        }
        if($this->step == 'leadscrew' && $this->substep == 'options'){
            $a = new LPC_linear_le_step_leadscrew_options_template();
            $a->side_bar_template();
        }
        if($this->step == 'leadscrew' && $this->substep == 'modifications'){
            $a = new LPC_linear_le_step_leadscrew_modifications_template();
            $a->side_bar_template();
        }
        if($this->step == 'nut' && $this->substep == 'options'){
            $a = new LPC_linear_le_step_nut_options_template();
            $a->side_bar_template();
        }
        if($this->step == 'review' && $this->substep == 'configuration'){
            $a = new LPC_linear_le_step_review_template();
            $a->side_bar_template();
        }
    }
}
new LPC_linear_le_templates();
<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_linear_le_template_navigator {

    public function __construct(){
        $this->parameters = new LPC_parameters();
        
        $this->set_parameters();
        $this->init();
        $this->navigator();
    }

    public function set_parameters(){
        $this->step           = $this->parameters->parameter(STEP); 
        $this->substep        = $this->parameters->parameter(SUBSTEP); 
        $this->voltage        = $this->parameters->parameter(VOLTAGE);    
        $this->amperage       = $this->parameters->parameter(AMPERAGE);   
        $this->force          = $this->parameters->parameter(FORCE);     
        $this->speed          = $this->parameters->parameter(SPEED);   
        $this->series         = $this->parameters->parameter(SERIES); 
        $this->motor          = $this->parameters->parameter(MOTOR); 
        $this->screw          = $this->parameters->parameter(SCREW); 
        $this->screw_length   = $this->parameters->parameter(SCREW_LENGTH); 
        $this->screw_coating  = $this->parameters->parameter(SCREW_COATING); 
        $this->screw_finish   = $this->parameters->parameter(SCREW_FINISH); 
        $this->nut            = $this->parameters->parameter(NUT); 
    }

    public function init(){
        // use to initiate properties and methods of class extensions
    }

    public function navigator(){
        if($this->step == 'motor' && $this->substep == 'series'){
            $this->motor_series();
        }
        if($this->step == 'motor' && $this->substep == 'length'){
            $this->motor_length();
        }
        if($this->step == 'leadscrew' && $this->substep == 'options'){
            $this->leadscrew_options();
        }
        if($this->step == 'leadscrew' && $this->substep == 'modifications'){
            $this->leadscrew_modifications();
        }
        if($this->step == 'nut' && $this->substep == 'options'){
            $this->nut_options();
        }
        if($this->step == 'review' && $this->substep == 'configuration'){
            $this->configuration_review();
        }
    }

    public function motor_series(){
        return null;
    }

    public function motor_length(){
        return null;
    }

    public function leadscrew_options(){
        return null;
    }

    public function leadscrew_modifications(){
        return null;
    }

    public function nut_options(){
        return null;
    }

    public function configuration_review(){
        return null;
    }

}
<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_linear_le_step_nut_options_template extends LPC_linear_le_template_model {

    public function template_init(){
        
        $this->series       = param(SERIES);
        $this->motor        = param(MOTOR);
        $this->nut          = param(NUT);
        $this->screw        = param(SCREW);

        $this->motor_data = $_SESSION['linear_le_motor_data'];
        $this->screw_data = $_SESSION['linear_le_screw_data'][$this->screw];

        $this->diagram_container      =  new LPC_linear_le_product_diagram_container;
        $this->spec_container         =  new LPC_linear_le_product_spec_container;
        $this->nut_options_container  =  new LPC_linear_le_product_nut_options_container;
        $this->display                =  new LPC_linear_spec_display;
        $this->colors                 =  new LPC_colors;
    }    

    public function main_area_template(){
        echo $this->diagram_container->outer_container();
        echo $this->nut_options_container->outer_container();
        echo $this->spec_container->outer_container();
        echo $this->chart();
    }

    public function chart(){
        ob_start();
        ?>

        <section class="half">
            <header>
                <span class="configurator-label">Performance Chart</span>
            </header>
            <div class="inner-container" id="leadscrew-modifications-container" data-equalizer-watch>
                <div class="loading-container">
                    <div class="loading-spinner"><i class="fas fa-spinner fa-spin fa-5x"></i></div>
                    <div id="lpc-chart" style="height:400px; width:100%;"></div>
                </div>
            </div>
        </section>

        <?php 
        return ob_get_clean();
    }

    public function side_bar_template(){

        echo $this->sidebar_header('Select Lead Screw Nut');

        $available_nuts = explode(';', $this->screw_data['nut_attributes']);

        $c_id = 0;
        foreach($available_nuts as $id){
        
            $nut_data = $_SESSION['linear_le_nut_data'][$id];

            $arg = array(
                'title'    => $nut_data['name'],
                'subtitle' => '',
                'class'    => 'nut-select',
                'img_id'   => $nut_data['img_id'],
                'color' => $this->colors->color[$c_id],
            );

            $content = array(
                "Flange Type" => $nut_data['flange_type'],
                "Nut Style"   => $nut_data['nut_style'],
            );

            echo $this->product_container_template($id, $this->nut, $arg, $content);
            $c_id++;
        }
    }
}
<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Implementation class for screen template classes. Contains HTML snippets
 */
class LPC_linear_le_template_model{

    public $count = 0;

    public function __construct(){
        $this->parameters = new \LPC_parameters();
        $this->le_parameters = new LPC_linear_le_steps_and_parameters();
        $this->linear_le_products = new LPC_linear_le_products();
        $this->charts = new Lpc_linear_le_charts();
        $this->calculations = new LPC_linear_le_calculations();
        $this->colors = new \LPC_colors();
        $this->price_lead = new LPC_linear_le_price_leadtime;
        $this->template_init();
        $this->actions();
    }

    /**
     * Used to initiate template specific methods and properties
     *
     * @return void
     */
    public function template_init(){
        /** keep empty */
    }

    public function main_area_template(){
        echo "[MAIN]";
    }

    public function side_bar_template(){
        echo "[SIDEBAR]";
    }

    /**
     * Product Container Template that's used in sidebar to select products or configuratiions
     *
     * @param string $id
     * @param string $selected
     * @param array $arg def: {title, subtitle, class, price, leadtime, img_id, color_id}
     * @param array $content {title => value}
     * @return void - HTML code used as part of other templates
     */
    public function product_container_template($id, $selected, $arg = array(), $content = array('title' => 'value')){

        /* Defaults */
        $title     = (isset($arg['title'])) ? $arg['title'] : '';
        $subtitle  = (isset($arg['subtitle'])) ? $arg['subtitle'] : '';
        $class     = (isset($arg['class'])) ? $arg['class'] : '';
        $price     = (isset($arg['price'])) ? $arg['price'] : 0;
        $leadtime  = (isset($arg['leadtime'])) ? $arg['leadtime'] : 0;
        $img_id    = (isset($arg['img_id'])) ? $arg['img_id'] : 0;
        $color     = (isset($arg['color'])) ? $arg['color'] : '#AAAAAA';
        $will_work = (isset($arg['will_work'])) ? $arg['will_work'] : true;
    
        ob_start();
        ?>
        <section 
            class="product-container color-bar <?php echo $class ?> <?php if($selected == $id){echo 'active';} ?>" 
            id="<?php echo $id ?>" 
            style="border-top-color:<?php if($will_work){echo $color;}else{echo '#AAAAAA';} ?>"
            data-color="<?php echo $color ?>" 
            data-will-work="<?php echo $will_work ?>" 
        >
            <div class="title-cont">

                <span>
                    <span class="title"><?php echo $title ?></span>
                    <span class="subtitle"><?php echo $subtitle ?></span>
                </span>

                <span class="selected-check right <?php echo ($selected == $id) ? 'selected-check-show' : ''; ?>" >
                    <i class="fas fa-check-circle"></i> Selected
                </span>       

                <span class="availability right">
                    <i class="fas fa-shopping-cart"></i> Available Online
                </span>
            </div>
            <div class="img-cont">
                <?php echo wp_get_attachment_image($img_id, 'small'); ?><br />
            </div>
            <div class="info-cont">
                <?php if(isset($arg['price'])): ?>      
                <div class="price-leadtime">
                    <div><span class="info-label ">Price:</span><span class="info-value"><?php echo $price ?></span></div>
                    <div><span class="info-label cell small-6">Lead Time:</span><span class="info-value"><?php echo $leadtime ?></span></div>                           
                </div>                
                <?php endif; ?>
                <div class="product-spec">
                    <?php 
                    foreach($content as $title => $value){
                        echo '<div><span class="info-label">'.$title.':</span><span class="info-value">'.$value.'</span></div>';
                    }
                    ?>
                </div>
            </div>
        </section>
        <?php
        return ob_get_clean();
    }

    public function sidebar_header($label, $clear_class = null){
        ob_start();
        ?>
            <header class="selection-header">
                <div class="label-container">
                   <span class="configurator-label"><?php echo $label; ?>:</span> 
                </div>
                <?php if($clear_class != null ): ?>
                <div class="button-container">
                    <button class="button small round <?php echo $clear_class; ?>">CLEAR / SHOW ALL</button>
                </div>
                <?php endif; ?>
            </header>
        <?php
        return ob_get_clean();    
    }

    public function main_area_chart_section_template(){
        ob_start();
        ?>
            <section class="full" id="curves">
                <header>
                    <span class="configurator-label">Performance Chart</span>
                    <a class="configurator-info">
                        <span data-tooltip class="bottom" tabindex="1" title="Enter your Force/Speed requirements and available power.">
                            <i class="fas fa-question-circle"></i>
                        </span>
                    </a>
                    <!-- <a class="configurator-option right">Imperial <span class="drop-arrow"><i class="fas fa-angle-down"></i></span></a> -->
                </header>
                <div class="inner-container">
                    <?php echo $this->charts->template(); ?>
                </div>
            </section>
        <?php 
        return ob_get_clean();
    }

    public function main_area_dimensions_section_template(){
        ob_start();
        ?>
            <section class="half" id="dimensions">
                <header>
                    <span class="configurator-label">Dimensions</span>
                </header>
                <div class="inner-container">

                </div>
            </section>
        <?php 
        return ob_get_clean();
    }
    
    public function specifications_section_with_container($content = null){
        ob_start();
        ?>
        <section class="half" id="specifications">
            <?php echo $content; ?>
        </section>
        <?php 
        return ob_get_clean();
    }

    public function specifications_section($table_content = array()){
        ob_start();
            ?>
            <header>
                <span class="configurator-label">Product Specifications</span>
            </header>
            <div class="inner-container" id="spec-container">
                <?php foreach($table_content as $title => $value):?>
                    <div class="spec-item-line">
                        <span><?php echo $title; ?></span><span><?php echo $value; ?></span>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php 
        return ob_get_clean();
    }



    public function main_area_downloads_section_template(){
        ob_start();
        ?>
            <section class="full" id="downloads">
                <header>
                    <span class="configurator-label">Useful Documents</span>
                </header>
                <div class="inner-container">
                    <div class="download-item" id="download-item-1">
                        <span class="download-item-desc">Lin Engineering Motor Operating Specifications</span><a class="right">Download</a>
                    </div>
                    <div class="download-item" id="download-item-2">
                        <span class="download-item-desc">LE17 Data Sheets</span><a class="right">Download</a>
                    </div>
                </div>
            </section>
        <?php 
        return ob_get_clean();
    }

    public function actions(){
        add_action('lpc_sidebar_label', array($this, 'title'));
    }

    public function title(){
        
    }

    public function price(){
        echo $this->price_lead->get_price();
    }

    public function leadtime(){
        echo $this->price_lead->get_lead_time();
    }
}
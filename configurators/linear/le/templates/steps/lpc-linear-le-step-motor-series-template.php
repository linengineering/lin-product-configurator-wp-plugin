<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Lpc_linear_le_step_motor_series_template extends LPC_linear_le_template_model{

    public function template_init(){

        $this->series_motor_data     = $_SESSION['linear_le_motor_data'];
        $this->series_chart_data     = $_SESSION['linear_le_chart_data']['series'];
        $this->selected_category     = $this->parameters->parameter(SERIES);

        $this->diagram_container     =  new LPC_linear_le_product_diagram_container;
        $this->spec_container        =  new LPC_linear_le_product_spec_container;
        $this->display               =  new LPC_linear_spec_display;
    }    

    public function main_area_template(){

        echo $this->main_area_chart_section_template();
        echo $this->diagram_container->outer_container();
        echo $this->spec_container->outer_container();

    }

    public function product_container($id, $data, $color, $will_work){
        if($data['series_stock_status'] == 'instock'){

            $max_min_a = $this->calculations->min_max_amperage_series($data);

            $amperage = ($max_min_a['min'] == $max_min_a['max']) ? $max_min_a['max'] . ' Amp/Phase' : $max_min_a['min'] . ' to ' . $max_min_a['max'] . ' Amp/Phase';

            $arg = array(
                'title'     => $this->display->nema_size($data['nema_size']),
                'subtitle'  => '(' . $data['series_name'] . ' Series)',
                'class'     => 'series-select',
                'price'     => '$' . $this->series_price($data),
                'leadtime'  => $this->lead_time($data) . '+ Days',
                'img_id'    => $data['thumbnail_id'],
                'color'     => $color,
                'will_work' => $will_work,
            );

            $content = array(
                'Frame Size'       => $this->display->frame_size($data['frame_size']),
                'Step Angle'       => $this->display->step_angle($data['step_angle']),
                'Stack Lengths' => $this->get_product_lengths($data),
                'Max Current'     => $this->display->amp_range($max_min_a['min'], $max_min_a['max']),
            );

            echo $this->product_container_template($data['term_id'], $this->selected_category, $arg, $content);
        }
    }

    public function side_bar_template(){
        
        $will_work     = [];
        $will_not_work = [];
        
        foreach($this->series_chart_data as $series_id => $chart_data){
            if($chart_data['force']['will_work']){
                $will_work[$series_id] = $this->series_motor_data[$series_id];
            } else {
                $will_not_work[$series_id] = $this->series_motor_data[$series_id];
            }
        }

        /** Sort by motor length and amperage */
        array_multisort( array_column($will_work, 'nema_size'), SORT_ASC, $will_work);
        array_multisort( array_column($will_not_work, 'nema_size'), SORT_ASC, $will_not_work);
        
        echo $this->sidebar_header('Choose Motor Series', 'clear-series-selection');

        if(!empty($will_work)){
            echo '<hr>';
            echo '<div class="selection-label"><p>Recommended:</p></div>';

            foreach($will_work as $data){
                if($data['series_stock_status'] == 'instock'){
                    $id = $data['term_id'];
                    $this->product_container($id, $data, $this->series_chart_data[$id]['color'], true);
                }
            }
        }
        
        if(!empty($will_not_work)){
            echo '<hr>';
            echo '<div class="selection-label"><p>Outside Of Performance Range:</p></div>';
            foreach($will_not_work as $data){
                if($data['series_stock_status'] == 'instock'){
                    $id = $data['term_id'];
                    $this->product_container($id, $data, $this->series_chart_data[$id]['color'], false);
                }
            }
        }
    }

    /**
     * calculate min and max lenght
     */
    public function get_product_lengths($data){
        $lengths = array();
        foreach($data['products'] as $product){
            $lenght = (isset($product['dimension_a'])) ? $product['dimension_a'] : ''; 
            array_push($lengths, (float)$lenght);
        }
        $min = min($lengths);
        $max = max($lengths);
        return $this->display->motor_length_range($min, $max);
    }

    /**
     * Returns base price of the series motors
     *
     * @return $price
    */
    public function series_price($arr){

        $price = 1000; // start with a high number to find lowest price in array

        foreach($arr['products'] as $id => $value){
            if($price >= (float)$value['price']){
                $price = (float)$value['price'];
            }
        }
        /* return value float with 2 decimal points */
        return sprintf('%.2f', $price);
    }

    public function lead_time($arr){

        $lead_time = 1000; // start with a high number to find lowest leadtime in array

        foreach($arr['products'] as $id => $value){
            if($lead_time >= (int)$value['lead_time']){
                $lead_time = (int)$value['lead_time'];
            }
        }

        return (int)$lead_time;
    }

    public function title(){
        echo "Choose Motor Series:";
    }

}
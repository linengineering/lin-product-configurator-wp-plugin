<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_linear_le_step_review_template extends LPC_linear_le_template_model {

    public function template_init(){

        $this->product_data = $_SESSION['linear_le_motor_data'];

        $this->motor        = $this->product_data[param(SERIES)]['products'][param(MOTOR)];
        $this->leadscrew    = $_SESSION['linear_le_screw_data'][param(SCREW)];
        $this->end_finish   = $_SESSION['linear_le_screw_end_finish_data'][param(SCREW_FINISH)];
        $this->nut          = $_SESSION['linear_le_nut_data'][param(NUT)];

        $this->price_class  = new LPC_linear_le_price_leadtime;
        $this->price        = $this->price_class->get_price();
        $this->lead_time    = $this->price_class->get_lead_time();
        $this->display      = new LPC_linear_spec_display;

        $this->diagram_container                 =  new LPC_linear_le_product_diagram_container;
        $this->spec_container                    =  new LPC_linear_le_product_spec_container;
        $this->leadscrew_modifications_container =  new LPC_linear_le_product_leadscrew_modifications_container;
        $this->nut_options_container             =  new LPC_linear_le_product_nut_options_container;

        /**
         * array of custom cart meta values to be displayed during checkout process. Title is generated from key stripping 'lpc' and '_' characters and capitolizing first letters. 
         */
        $this->custom_meta    = array(
            'lpc_screw_type'    => $this->leadscrew['name'],
            'lpc_screw_length'  => param(SCREW_LENGTH) . ' mm',
            'lpc_end_machining' => $this->end_finish['name'],
            'lpc_nut'           => $this->nut['name'],
        );

        $this->order_email_meta = array(
            'BOM SAP#'    => $this->motor['sap_number'],
            'Config Name' => 'Lin_Config_LE'.$this->motor['nema_size'] ,
            'Motor'       => $this->motor['name'],
            'Screw'       => $this->leadscrew['desc'],
            'Length'      => param(SCREW_LENGTH),
            'Finish'      => $this->end_finish['name'],
            'Nut'         => $this->nut['desc'],
        );


        $query_string = filter_var($_SERVER['QUERY_STRING'], FILTER_SANITIZE_URL);
        $_SESSION['order_email_meta'] = $this->order_email_meta;
        $_SESSION['item_permalink'] = '/rapid-prototype-configurator?' . $query_string;
        $this->cart_actions();
    }

    public function main_area_template(){

        echo $this->spec_container->outer_container();
        echo $this->chart();        
        echo $this->dimensions();
        echo $this->leadscrew_modifications_container->outer_container();
        echo $this->nut_options_container->outer_container();

    }

    public function dimensions(){
        ob_start();
        ?>
        <section class="full" id="dimensions">
            <header>
                <span class="configurator-label">Motor Dimensions</span>
            </header>
            <div class="inner-container" id="dim-container">
                <div class="grid-x">
                    <div class="cell small-6">
                        <?php echo wp_get_attachment_image($this->motor['dim_front_img_id'], 'full') ?> 
                    </div>
                    <div class="cell small-6">
                        <?php echo wp_get_attachment_image($this->motor['dim_side_img_id'], 'full')  ?> 
                    </div>
                    <div class="cell small-12">
                        <div class="spec-item-line">
                            <span>Dimension A:</span><span><?php echo $this->display->motor_length($this->motor['dimension_a']); ?></span>
                        </div>
                        <div class="spec-item-line">
                            <span>Dimension L:</span><span><?php echo $this->display->leadscrew_length(param(SCREW_LENGTH)); ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php 
        return ob_get_clean();
    }

    public function chart(){
        ob_start();
        ?>

        <section class="half">
            <header>
                <span class="configurator-label">Performance Chart</span>
            </header>
            <div class="inner-container" id="leadscrew-modifications-container" data-equalizer-watch>
                <div class="loading-container">
                    <div class="loading-spinner"><i class="fas fa-spinner fa-spin fa-5x"></i></div>
                    <div id="lpc-chart" style="height:400px; width:100%;"></div>
                </div>
            </div>
        </section>

        <?php
        return ob_get_clean();
    }

    public function side_bar_template(){

        // key is used for id and name
        $selections = array(
            'motor'            => $this->motor['name'],
            'screw'            => $this->leadscrew['name'],
            'screw_length'     => $this->display->leadscrew_length(param(SCREW_LENGTH)),
            'screw_end_finish' => $this->end_finish['name'],
            'nut'              => $this->nut['name'],
        );

        ob_start();
        ?> 
        <div class="review-sidebar">
            <h3>Selected Components:</h3>
            <?php foreach($selections as $id => $selection): ?>
            <div class="container">
                <div class="config">
                    <p class="title"><?php echo ucwords(str_replace('_', ' ', $id)); ?></p>
                    <p class="desc"><?php echo $selection; ?></p>
                </div>
                <div class="edit">
                    <button class="button small round right" id="edit_<?php echo $id; ?>">edit</button>
                </div>
            </div>
            <?php endforeach; ?>

            <div class="add-to-cart-container">
                <div class="cart-qty-label">
                    <label for="middle-label" class="text-right middle">Qty</label>
                </div>
                <div class="cart-qty">
                    <input type="number" name="quantity" min="1" max="5" value="1">
                </div>

                <div class="cart-button">
                   <button class="button round success expanded" id="add-to-cart"><i class="fas fa-shopping-cart"></i> Add To Cart</button>  
                </div>

                <span class="cart-notice">To ensure fast prototype turnaround, order quantity is limited to 5 prototypes of the same configuration. For higher quantity orders, please email salesteam@linengineering.com. </span>

            </div>


        </div>
        <?php 
        echo ob_get_clean();
    }       

    public function add_to_cart(){
        global $woocommerce;
        $qty = isset($_POST['order_qty']) ? (int)sanitize_key($_POST['order_qty']) : 1;
        $woocommerce->cart->add_to_cart(param(MOTOR), $qty);
    }

    public function cart_message(){
        ob_start();
        ?>
            <h3><?php echo $this->motor['name']; ?> Added To Cart</h3>
            <p class="lead">Would You Like To View Cart?</p>
            <a class="button success" id="add-to-cart" style="color:white;" href="/cart"><i class="fas fa-shopping-cart"></i> View Cart</a>
            <button class="button" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">Revise Configuration</span>
            </button>
            <button class="close-button" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">&times;</span>
            </button>
        <?php 
        return ob_get_clean();
    }

    public function cart_actions(){
        add_filter('woocommerce_add_cart_item_data', array($this, 'add_cart_item_data'), 10, 2 );
    }

    public function add_cart_item_data($cart_item_data, $product_id){
        foreach($this->custom_meta as $key => $value){
            $cart_item_data[$key] = $value;
        }
        $cart_item_data['config_price'] = $this->price;
        return $cart_item_data;
    }

}
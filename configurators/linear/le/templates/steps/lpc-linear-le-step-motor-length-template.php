<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Lpc_linear_le_step_motor_length_template extends LPC_linear_le_template_model {


    public function template_init(){
        $this->series     = param(SERIES);
        $this->motor      = param(MOTOR);

        $this->motor_data            = $_SESSION['linear_le_motor_data'][$this->series]['products'];
        $this->chart_data            = $_SESSION['linear_le_chart_data']['series'][$this->series]['motors'];

        $this->diagram_container     =  new LPC_linear_le_product_diagram_container;
        $this->spec_container        =  new LPC_linear_le_product_spec_container;
        $this->display               =  new LPC_linear_spec_display;
    }    

    public function main_area_template(){
        echo $this->main_area_chart_section_template();
        echo $this->diagram_container->outer_container();
        echo $this->spec_container->outer_container();
    }

    public function product_container($id, $data, $color, $will_work){
        $arg = array(
            'title'     => $data['name'],
            'subtitle'  => '',
            'class'     => 'motor-select',
            'img_id'    => get_post_thumbnail_id($id),
            'color'     => $color,
            'will_work' => $will_work,
        );

        $content = array(
            'Length (Dim A)'   => $this->display->motor_length($data['dimension_a']),
            'Frame Size'       => $this->display->frame_size($data['frame_size']),
            'Step Angle'       => $this->display->step_angle($data['step_angle']),
            'Max Current'      => $this->display->amp($data['current']),
        );

        echo $this->product_container_template($id, $this->motor, $arg, $content);
    }

    public function side_bar_template(){

        $will_work     = [];
        $will_not_work = [];
        
        foreach($this->chart_data as $id => $data){
            if($data['force']['will_work']){
                $will_work[$id] = $this->motor_data[$id];
            } else {
                $will_not_work[$id] = $this->motor_data[$id];
            }
        }

        /** Sort by motor length and amperage */
        array_multisort( array_column($will_work, 'dimension_a'), SORT_ASC, array_column($will_work, 'current'), SORT_ASC, $will_work);
        array_multisort( array_column($will_not_work, 'dimension_a'), SORT_ASC, array_column($will_not_work, 'current'), SORT_ASC, $will_not_work);

        echo $this->sidebar_header('Select Motor Type', 'clear-motor-selection');

        if(!empty($will_work)){
            echo '<hr>';
            echo '<div class="selection-label"><p>Recommended:</p></div>';
            foreach($will_work as $data){
                if($data['stock_status'] == 'instock'){
                    $id = $data['term_id'];
                    $this->product_container($id, $data, $this->chart_data[$id]['color'], true);
                }
            }
        }

        if(!empty($will_not_work)){
            echo '<hr>';
            echo '<div class="selection-label"><p>Outside Of Performance Range:</p></div>';
            foreach($will_not_work as $data){
                if($data['stock_status'] == 'instock'){
                    $id = $data['term_id'];
                    $this->product_container($id, $data, $this->chart_data[$id]['color'], false);
                }
            }
        }

    }
}
<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_linear_le_step_leadscrew_modifications_template extends LPC_linear_le_template_model {

    public function template_init(){

        $this->series       = param(SERIES);
        $this->motor        = param(MOTOR);
        $this->screw        = param(SCREW);
        $this->screw_length = param(SCREW_LENGTH);
        $this->screw_finish = param(SCREW_FINISH);
        
        $this->motor_data            = $_SESSION['linear_le_motor_data'][$this->series]['products'][$this->motor];
        $this->screw_data            = $_SESSION['linear_le_screw_data'][$this->screw];
        $this->screw_end_finish_data = $_SESSION['linear_le_screw_end_finish_data'];
        $this->chart_data            = $_SESSION['linear_le_chart_data']['series'][$this->series]['motors'][$this->motor]['screws'][$this->screw];

        $this->diagram_container                 =  new LPC_linear_le_product_diagram_container;
        $this->spec_container                    =  new LPC_linear_le_product_spec_container;
        $this->leadscrew_modifications_container =  new LPC_linear_le_product_leadscrew_modifications_container;
        $this->display                           =  new LPC_linear_spec_display;
        $this->colors                            =  new LPC_colors;

    }    

    public function main_area_template(){
        echo $this->diagram_container->outer_container();
        echo $this->leadscrew_modifications_container->outer_container();
        echo $this->spec_container->outer_container();
        echo $this->chart();
    }

    public function chart(){
        ob_start();
        ?>

        <section class="half">
            <header>
                <span class="configurator-label">Performance Chart</span>
            </header>
            <div class="inner-container" id="leadscrew-modifications-container" data-equalizer-watch>
                <div class="loading-container">
                    <div class="loading-spinner"><i class="fas fa-spinner fa-spin fa-5x"></i></div>
                    <div id="lpc-chart" style="height:400px; width:100%;"></div>
                </div>
            </div>
        </section>

        <?php 
        return ob_get_clean();
    }

    public function side_bar_template(){

        echo $this->sidebar_header('Lead Screw Modification Options');

        if(isset($this->motor_data)){

            $data   = $this->motor_data['attributes']['screw-mods']['options'];
            $length = ((float)$this->screw_length > 0) ? $this->screw_length : 300;

            ob_start();
            ?> 
            <hr>
            <div class="leadscrew-length" style="margin: 1rem;">
                <p style="color: #333; font-weight:500;">Lead Screw Length (mm):</p>
                <div class="grid-x grid-margin-x">
                    <div class="cell small-3">
                        <input type="number" step="any" id="screw_length">
                    </div>                
                    <div class="cell small-6">
                        <div class="slider" data-slider data-initial-start="<?php echo $length; ?>" data-start="20" data-end="300" data-step="0.1" data-decimal="1">
                            <span class="slider-handle" data-slider-handle role="slider" tabindex="1" aria-controls="screw_length"></span>
                            <span class="slider-fill" data-slider-fill></span>
                        </div>
                    </div>
                    <div class="cell small-3">
                        <button class="button round expanded" id="enter_screw_length">ENTER</button>
                    </div>
                </div>
                <span style="font-size:0.8rem; color:#8a8a8a;"><i>Standard lead screw length is 300mm (11.81").</i></span><br>
                <span style="font-size:0.8rem; color:#8a8a8a">Length Modification Price:</span>
                <span style="font-size:0.85rem; color:#44474c; font-weight:500;">+$15</span> 
                <span style="font-size:0.8rem; color:#8a8a8a; margin-left: 1rem;">Lead Time:</span>
                <span style="font-size:0.85rem; color:#44474c; font-weight:500;">+5 days</span>       
            </div>
              
            <hr>

            <?php 
            echo ob_get_clean();
        }        

        echo '<div class="selection-label"><p>Screw End Machining:</p></div>';

        $available_end_finish = explode(';', $this->screw_data['finish_attributes']);
        $c_id = 0;
        foreach($available_end_finish as $id){

            $end_finish_data = $this->screw_end_finish_data[$id];

            $arg = array(
                'title'    => $end_finish_data['name'],
                'subtitle' => '',
                'class'    => 'end-finish-select',
                'price'    => '+$'.$end_finish_data['price'],
                'leadtime' => '+' . $end_finish_data['lead_time'] . ' days',
                'img_id'   => $end_finish_data['img_id'],
                'color'    => $this->colors->color[$c_id],
            );

            $content = array(
            );

            echo $this->product_container_template($id, $this->screw_finish, $arg, $content);
            $c_id++;
        }

    }

}
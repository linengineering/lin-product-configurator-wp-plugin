<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_linear_le_product_spec_container extends LPC_linear_le_template_navigator {

    public function init(){
        $this->table    = array();
        $this->product_class  = new LPC_linear_le_products;
        $this->display = new LPC_linear_spec_display;
        $this->products = $_SESSION['linear_le_motor_data'];
    }

    public function motor_series(){
        if($this->series == ''){
            $this->table = [array(
                'title' => '',
                'content' => array(
                  'Note' => 'Please Select a Motor Series',  
                )
            )];
        } else {
            $this->table = [$this->series_table()];
        }
    }

    public function motor_length(){
        if($this->motor == ''){
            $this->table = [$this->series_table()];
        } else {
            $this->table = [$this->motor_table()];
        }
    }

    public function leadscrew_options(){
        if($this->screw == ''){
            $this->table = [$this->motor_table()];
        } else {
            $this->table = [$this->motor_table(), $this->screw_table()];
        }
    }

    public function leadscrew_modifications(){
        $this->table = [$this->motor_table(), $this->screw_table()];
    }

    public function nut_options(){
        $this->table = [$this->motor_table(), $this->screw_table()];
    }

    public function configuration_review(){
        $this->nut_options();
    }

    public function series_table(){
        $data = $this->products[$this->series];
        $array = array(
            'title'   => 'Selected Series',
            'content' => array(
                'Series'  => $data['series_name'],
                'NEMA Size'  => $this->display->nema_size($data['nema_size']),
                'Frame Size' => $this->display->frame_size($data['frame_size']),
                'Step Angle' => $this->display->step_angle($data['step_angle']),
            )
        );
        return $array;
    }

    public function motor_table(){
        $data = $this->products[$this->series]['products'][$this->motor];
        $array = array(
            'title'   => 'Motor Specifications',
            'content' => array(
                'Part Number'     => $data['name'],
                'NEMA Size'       => $this->display->nema_size($data['nema_size']),
                'Frame Size'      => $this->display->frame_size($data['frame_size']),
                'Step Angle'      => $this->display->step_angle($data['step_angle']),
                'Holding Torque'  => $this->display->holding_torque($data['holding_torque']),
                'Rated Current'   => $this->display->amp($data['current']),
                'Resistance'      => $this->display->resistance($data['resistance']),
                'Inertia'         => $this->display->inertia($data['inertia']),
                'Winding'         => ucfirst($data['winding']),
                'No of Leads'     => $data['no_of_leads'],
            )
        );
        return $array;
    }

    public function screw_table(){
        $motor_data = $this->products[$this->series]['products'][$this->motor];
        foreach($motor_data['attributes']['screw']['options'] as $id => $name){
            if($id == $this->screw){
                $screw_data = $_SESSION['linear_le_screw_data'][$id];
            }
        }
        $array = array(
            'title'   => 'Leadscrew Specifications',
            'content' => array(
                'Lead' => $this->display->leadscrew_lead($screw_data['lead']),
                'OD'   => $this->display->leadscrew_od($screw_data['od']),
            )
        );
        return $array;
    }

    public function screw_mod_table(){
        return array();
    }

    public function nut_table(){
        return array();
    }


    /**
     * Table inner container for AJAX calls
     *
     * @return string
     */
    public function inner_container(){
        $loaders = new LPC_loader_templates;
        ob_start();
            ?>
            <header>
                <span class="configurator-label">Motor Specifications</span>
            </header>
            <div class="inner-container" id="spec-container" data-equalizer-watch>
                <?php echo $loaders->loader_spinner(); ?>
                <?php 
                foreach($this->table as $table){
                    if(!empty($table['title'])){
                        echo '<span style="margin-top:1rem;"><strong>'.$table['title'].':</strong></span>';
                    } 
                    if(!empty($table['content'])){
                        foreach($table['content'] as $title => $value){ 
                        ?>
                            <div class="spec-item-line">
                                <span><?php echo $title; ?>:</span><span><?php echo $value; ?></span>
                            </div>
                        <?php
                        }
                    }                    
                }
                ?>
            </div>
            <?php 
        return ob_get_clean();
    }

    /**
     * Table with outer container to use in initial template
     *
     * @return string
     */
    public function outer_container(){
        ob_start();
        ?>
        <section class="half" id="motor_specifications">
            <?php echo $this->inner_container(); ?>
        </section>
        <?php 
        return ob_get_clean();
    }

}
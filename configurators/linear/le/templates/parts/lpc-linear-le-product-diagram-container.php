<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_linear_le_product_diagram_container extends LPC_linear_le_template_navigator {

    public function init(){
        $this->img_id   = 0;
        $this->dim      = array();
        $this->product_class  = new LPC_linear_le_products;
        $this->display  = new LPC_linear_spec_display;
        $this->products = $_SESSION['linear_le_motor_data'];
    }

    public function motor_series(){
        if($this->series == ''){
            $this->img_id = 0;
            $this->dim = array(
                'Note' => 'Please Select a Motor Series',
            );
        } else {
            $data = $this->products[$this->series];
            $this->img_id = $data['dim_front_img_id'];
        }
    }

    public function motor_length(){
        if($this->motor == ''){
            $data = $this->products[$this->series];
            $this->img_id = $data['dim_side_img_id'];
            $this->dim    = array(
                "Dimension A" => 'Select Motor',
            );
        } else {
            $data = $this->products[$this->series]['products'][$this->motor];
            $this->img_id = $data['dim_side_img_id'];
            $this->dim    = array(
                "Dimension A" => $this->display->motor_length($data['dimension_a']),
                "Dimension L (Lx + Lu)" => "TBD",
            );
        }
    }

    public function leadscrew_options(){
        if($this->screw == ''){
            $data = $this->products[$this->series]['products'][$this->motor];
            $this->img_id = $data['dim_side_img_id'];
            $this->dim    = array(
                "Dimension A" => $this->display->motor_length($data['dimension_a']),
                "Dimension L (Lx + Lu)" => "TBD",
            );
        } else {
            $motor_data = $this->products[$this->series]['products'][$this->motor];
            $screw_data = array('od' => 0);
            foreach($motor_data['attributes']['screw']['options'] as $id => $name){
                if($id == $this->screw){
                    $screw_data = $_SESSION['linear_le_screw_data'][$id];
                }
            }
            $this->img_id = $motor_data['dim_side_img_id'];
            $this->dim    = array(
                "Dimension A" => $this->display->motor_length($motor_data['dimension_a']),
                "Dimension L (Lx + Lu)" => "TBD",
            );
        }
    }

    public function leadscrew_modifications(){
        $motor_data = $this->products[$this->series]['products'][$this->motor];
        $screw_data = array('od' => 0);
        foreach($motor_data['attributes']['screw']['options'] as $id => $name){
            if($id == $this->screw){
                $screw_data = $_SESSION['linear_le_screw_data'][$id];
            }
        }
        $this->img_id = $motor_data['dim_side_img_id'];
        $this->dim    = array(
            "Dimension A" => $this->display->motor_length($motor_data['dimension_a']),
            "Dimension L (Lx + Lu)" => $this->display->leadscrew_length(param(SCREW_LENGTH)),
            // "Dimension Ly" => '',
            // "Usable Thread" => '',
        );
    }

    public function nut_options(){
        $this->leadscrew_modifications();
    }

    public function configuration_review(){
        $this->leadscrew_modifications();
    }

    /**
     * Table inner container for AJAX calls
     *
     * @return string
     */
    public function inner_container(){
        $loaders = new LPC_loader_templates;
        ob_start();
            ?>
            
            <header>
                <span class="configurator-label">Motor Dimensions</span>
            </header>
            <div class="inner-container" id="dim-container" data-equalizer-watch>
                <?php echo $loaders->loader_spinner(); ?>
                <?php if($this->img_id != 0): ?>
                <div class="dim-value">
                    <div class="dim-img">
                        <?php echo wp_get_attachment_image($this->img_id, 'full'); ?> 
                    </div>
                </div>
                <?php endif; ?>
                
                <?php if(!empty($this->dim)): ?>
                <div class="dim-value">
                    <?php foreach($this->dim as $title => $value):?>
                        <div class="spec-item-line">
                            <span><?php echo $title; ?>:</span><span><?php echo $value; ?></span>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>

            </div>
            <?php 
        return ob_get_clean();
    }

    /**
     * Table with outer container to use in initial template
     *
     * @return string
     */
    public function outer_container(){
        ob_start();
        ?>
        <section class="half" id="motor_dimensions" data-step="4" data-intro="Dimensions and Specifications will appear in this area after making a selection.">
            <?php echo $this->inner_container(); ?>
        </section>
        <?php 
        return ob_get_clean();
    }

}
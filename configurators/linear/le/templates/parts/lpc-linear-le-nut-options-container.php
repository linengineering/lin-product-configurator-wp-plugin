<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_linear_le_product_nut_options_container extends LPC_linear_le_template_navigator {

    public function init(){
        $this->table          = array();
        $this->product_class  = new LPC_linear_le_products;
        $this->display        = new LPC_linear_spec_display;
        $this->products       = $_SESSION['linear_le_motor_data'];

        $this->img_id = 0;
        $this->dim = array();
        $this->heading = '';
    }

    public function nut_options(){
        if(!empty($this->nut)){

            $data = $_SESSION['linear_le_nut_data'][$this->nut];
            $this->img_id = $data['img_id'];
            $this->heading = $data['name'] . ' - ' . $data['flange_type'] . ', ' . $data['nut_style'] . ' Nut';
            $this->dim = array(
        );

        } else {
            $this->dim = array(
                'Note' => 'Please select nut option',
            );
        }
    }

    public function configuration_review(){
        $this->nut_options();
    }

    /**
     * Table inner container for AJAX calls
     *
     * @return string
     */
    public function inner_container(){
        $loaders = new LPC_loader_templates;
        ob_start();
            ?>
            <header>
                <span class="configurator-label">Nut Dimensions</span>
            </header>
            <div class="inner-container" id="nut-modifications-container" data-equalizer-watch>
                <h4><?php echo $this->heading ?></h4>
                <?php if($this->img_id != 0): ?>
                    <?php echo $loaders->loader_spinner(); ?>
                    <div class="dim-value">
                        <div class="dim-img">
                            <?php echo wp_get_attachment_image($this->img_id, 'full') ?> 
                        </div>
                    </div>
                    <?php endif; ?>
                    
                    <?php if(!empty($this->dim)): ?>
                    <div class="dim-value">
                        <?php foreach($this->dim as $title => $value):?>
                            <div class="spec-item-line">
                                <span><?php echo $title; ?>:</span><span><?php echo $value; ?></span>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
            <?php 
        return ob_get_clean();
    }

    /**
     * Table with outer container to use in initial template
     *
     * @return string
     */
    public function outer_container(){
        ob_start();
        ?>
        <section class="half" id="nut-modifications">
            <?php echo $this->inner_container(); ?>
        </section>
        <?php 
        return ob_get_clean();
    }

}
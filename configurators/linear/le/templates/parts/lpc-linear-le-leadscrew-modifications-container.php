<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_linear_le_product_leadscrew_modifications_container extends LPC_linear_le_template_navigator {

    public function init(){
        $this->table          = array();
        $this->product_class  = new LPC_linear_le_products;
        $this->display        = new LPC_linear_spec_display;
        $this->products       = $_SESSION['linear_le_motor_data'];

        $this->img_id  = 0;
        $this->dim     = array();
        $this->heading = '';
    }

    public function leadscrew_modifications(){
        if(!empty($this->screw_finish)){

            $data = $_SESSION['linear_le_screw_end_finish_data'][$this->screw_finish];
            $this->img_id = $data['img_id'];

            $slug_key = substr($data['slug'], 0, 4);

            if($slug_key == 'no-e'){
                $this->heading = $data['name'];
                $this->dim = array(
                    'Dimensions' => 'N/A',
                );
            }     
            
            if($slug_key == 'th_e'){
                $this->heading = $data['name'];
                $this->dim = array(
                    'Dimension U' => $data['dim_u'] . '"',
                    'Dimension T' => $data['dim_t'],
                );
            }  
            
            if($slug_key == 'th_m'){
                $this->heading = $data['name'];
                $this->dim = array(
                    'Dimension U' => $data['dim_u'] . 'mm',
                    'Dimension T' => $data['dim_t'],
                );
            }    
            
            if($slug_key == 'pb_e' || $slug_key == 'pb_t'){
                $this->heading = $data['name'];
                $this->dim = array(
                    'Dimension U' => $data['dim_u'] . ' mm',
                    'Dimension T' => $data['dim_t'] . ' mm',
                );
            }      

            if($slug_key == 'pb_g'){
                $this->heading = $data['name'];
                $this->dim = array(
                    'Dimension U' => sprintf('%0.2f mm', $data['dim_u']),
                    'Dimension T' => sprintf('%0.2f mm', $data['dim_t']),
                    'Dimension V' => sprintf('%0.2f mm', $data['dim_v']),
                    'Dimension X' => sprintf('%0.2f mm', $data['dim_x']),
                    'Dimension Y' => sprintf('%0.2f mm', $data['dim_y']),
                );
            }

        } else {
            $this->dim = array(
                'Note' => 'Please select end finish',
            );
        }
    }

    public function configuration_review(){
        $this->leadscrew_modifications();
    }

    /**
     * Table inner container for AJAX calls
     *
     * @return string
     */
    public function inner_container(){
        $loaders = new LPC_loader_templates;
        ob_start();
            ?>
            <header>
                <span class="configurator-label">Leadscrew End Finish</span>
            </header>
            <div class="inner-container" id="leadscrew-modifications-container" data-equalizer-watch>
                <h4><?php echo $this->heading ?></h4>
                <?php if($this->img_id != 0): ?>
                    <?php echo $loaders->loader_spinner(); ?>
                    <div class="dim-value">
                        <div class="dim-img">
                            <?php echo wp_get_attachment_image($this->img_id, 'full') ?> 
                        </div>
                    </div>
                    <?php endif; ?>
                    
                    <?php if(!empty($this->dim)): ?>
                    <div class="dim-value">
                        <?php foreach($this->dim as $title => $value):?>
                            <div class="spec-item-line">
                                <span><?php echo $title; ?>:</span><span><?php echo $value; ?></span>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
            <?php 
        return ob_get_clean();
    }

    /**
     * Table with outer container to use in initial template
     *
     * @return string
     */
    public function outer_container(){
        ob_start();
        ?>
        <section class="half" id="leadscrew-modifications">
            <?php echo $this->inner_container(); ?>
        </section>
        <?php 
        return ob_get_clean();
    }

}
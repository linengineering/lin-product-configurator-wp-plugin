<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * This class handles all script and styles associated with Linear LE Configurator 
 */
class LPC_linear_le_enqueue_scripts {

    public function enqueue_scripts(){
        add_action('wp_enqueue_scripts', array($this, 'scripts_to_enqueue'));
    }

    public function scripts_to_enqueue(){

        /** Enqueue Linear LE main */
        wp_enqueue_script( 'lpc-linear-le-main', LPC_LINEAR_LE_URI . 'assets/js/lpc-linear-le-main.js', array('lpc-ajax'), '', true );

        /** Enqueue Linear LE Chart scripts */
        wp_enqueue_script( 'lpc-linear-le-chart', LPC_LINEAR_LE_URI . 'assets/js/lpc-linear-le-chart.js', array('lpc-linear-le-main'), '', true );
        
    }
}
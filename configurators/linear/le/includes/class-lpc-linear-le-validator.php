<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_linear_le_validator {

    public function __construct(){
        $this->steps_and_parameters = new \LPC_linear_le_steps_and_parameters();
    }

    /**
     * Array indicates if step has been completed or not
     *
     * @return array
     */
    public function completed_steps(){
        $steps = $this->steps_and_parameters->steps;

        $completed_array = [];
        /* Check if step is complete */
        foreach($steps as $step => $step_data){
            foreach($step_data['substeps'] as $substep => $substep_data ){
                $validator = $this->validate($step, $substep);
                $completed_array[$step][$substep] = $validator;
            }
        }
        return $completed_array;
    }

    public function validate($step, $substep){
        if($step == 'motor' && $substep == 'series'){
            return $this->motor_series();
        }
        
        if($step == 'motor' && $substep == 'length'){
            return $this->motor_length();
        }
        
        if($step == 'leadscrew' && $substep == 'options'){
            return $this->leadscrew_options();
        }
        
        if($step == 'leadscrew' && $substep == 'modifications'){
            return $this->leadscrew_modifications();
        }

        if($step == 'nut' && $substep == 'options'){
            return $this->nut_options();
        }

        if($step == 'review' && $substep == 'configuration'){
            return $this->review_configuration();
        }
    }

    public function motor_series(){
        if(!empty(param(SERIES))){
            return true;
        } else {
            return false;
        }
    }

    public function motor_length(){
        if(!empty(param(MOTOR))){
            return true;
        } else {
            return false;
        }
    }

    public function leadscrew_options(){
        if(!empty(param(SCREW))){
            return true;
        } else {
            return false;
        }
    }

    public function leadscrew_modifications(){
        if(!empty(param(SCREW_FINISH))){
            return true;
        } else {
            return false;
        }
    }

    public function nut_options(){
        if(!empty(param(NUT))){
            return true;
        } else {
            return false;
        }
    }

    public function review_configuration(){
        return false;
    }

}
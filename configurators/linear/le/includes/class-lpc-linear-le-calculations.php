<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_linear_le_calculations {

    public $nut_cof = 0.15;  // Required input

    public function __construct(){
        $this->products       = new LPC_linear_le_products();
        $this->parameters     = new LPC_parameters();
    }

    /**
     * calculates and returns coordinates in array form of 'speed vs force' data points
     *
     * @param array $torque_curve
     * @param int $diameter
     * @param int $lead
     * @param int $root_diameter
     * @return array
     */
    public function speed_vs_force($torque_curve, $diameter, $lead, $root_diameter){

        $diameter = $diameter / 25.4;
        $lead = $lead / 25.4;
        $root_diameter = $root_diameter / 25.4;

        $linear_speed_vs_force = array();
        
        $pitch_diameter = $this->pitch_diameter($diameter, $root_diameter);
        $helix_angel    = $this->helix_angle($lead, $pitch_diameter);
        $efficiency     = $this->efficiency($helix_angel, $this->nut_cof);
        $linear_speed   = $this->linear_speed($torque_curve, $lead); //returns array
        $load           = $this->load($torque_curve, $efficiency, $lead); //returns array

        $i = 0;
        foreach ($linear_speed as $key => $value) {
          $data = array( 'x' => $value, 'y' => $load[$i]);
          array_push($linear_speed_vs_force, $data);
          $i++;
        }

        return $linear_speed_vs_force;
    }

    /**
     * Calculates pitch diameter based on diameter and root diameter
     *
     * @param int $diameter
     * @param int $root_diameter
     * @return int
     */
    public function pitch_diameter($diameter, $root_diameter){
        $pitch_diameter = ($diameter + $root_diameter) / 2;
        return $pitch_diameter; 
    }

    /**
     * Calculates helis_angle based on screw's lead and pitche diameter
     *
     * @param int $lead
     * @param int $pitch_diameter
     * @return int
     */
    public function helix_angle($lead, $pitch_diameter){
        $helix_angle = rad2deg( atan( $lead / ( pi() * $pitch_diameter ) ) );
        return $helix_angle;
    }

    public function efficiency($helix_angle, $nut_cof){
        $efficiency = (tan(deg2rad($helix_angle)) / (tan(deg2rad($helix_angle) + atan($nut_cof)))) * 100;
        return $efficiency;
    }

    public function linear_speed($torque_curve, $lead){
        $linear_speed = array();
        foreach ($torque_curve as $key => $value) {
            $speed_calc = ($value['x'] * $lead) / 60;
            array_push($linear_speed, round($speed_calc, 2));
        }
        return $linear_speed;
    }

    public function load($torque_curve, $efficiency, $lead){
        $load = array();
        foreach ($torque_curve as $key => $value) {
            $x = $value['x'];
            $y = $value['y'];
            $load_calc = (2 * pi() * $efficiency / 100 * $value['y'] / 16) / $lead;
            array_push($load, round($load_calc, 2));
        }
        return $load;
    }

    public function torque_amp_v($torque_curve, $base_voltage, $base_amperage, $new_voltage, $new_amperage){

        $new_torque_curve = array();

        foreach($torque_curve as $key => $value){
            $new_force = ($new_amperage/$base_amperage) * $value['y'];
            $new_speed = ($new_voltage/$base_voltage) * $value['x'];
            $new_torque_dataset = array('x' => $new_speed, 'y' => $new_force);
            array_push($new_torque_curve, $new_torque_dataset);
        }
        
        return $new_torque_curve;
    }

    public function torque_decoder($torque_curve)
    {
        $arr = explode(';', $torque_curve); 
        $val = array();
        foreach($arr as $value)
        {
            $point = explode(':', $value);
            if(isset($point[1]))
            {
                array_push($val, array('x' => floatval($point[0]) , 'y' => floatval($point[1]) ));
            }
        }
        return($val);
    }

    public function torque_calculator($product_data){

        $torque_array  = $this->torque_decoder($product_data['torque_array']);

        $selected_voltage   = (float)$this->parameters->parameter(VOLTAGE);
        $selected_amperage  = (float)$this->parameters->parameter(AMPERAGE);

        $base_voltage  = (float)$product_data['tested_v'];
        $base_amperage = (float)$product_data['tested_a'];        

        /**
         * Prevent generating torque curves based on amperage greater than rated amperage.
         */
        if($selected_amperage > $base_amperage){
            $new_amperage = $base_amperage;
        } else {
            $new_amperage = $selected_amperage;
        }

        $new_voltage = $selected_voltage;

        if($new_voltage > 0 && $new_amperage > 0){
            $new_torque_array = $this->torque_amp_v($torque_array, $base_voltage, $base_amperage, $new_voltage, $new_amperage);
            return $new_torque_array;
        } else {
            return $torque_array;
        }
    } 

    public function leadscrew_calculation($torque, $leadscrew_data){
        return $this->speed_vs_force($torque, (float)$leadscrew_data['od'], (float)$leadscrew_data['lead'], (float)$leadscrew_data['root_od']);
    }

    /**
     * User for usort function. Sorts datapoints by x axis value in ascending order
     */
    public function sort_x_axis($a, $b){
        if($a['x'] > $b['x']){
            return -1;
        } elseif ($a['x'] < $b['x']){
            return 1;
        }
    }

    /**
     * Filters array of datapoints and returns array with highest y axis values for each x axis point. 
     *
     * @param array $points
     * @return array 
     */
    public function sort_high_y($points){

        $to_sort = $points; 
        usort($to_sort, array($this, 'sort_x_axis'));

        $x = 0;
        $y = 0;

        $buffer = ($to_sort[0]['x'] / 150);

        $sorted = array();

        foreach($to_sort as $key => $value){

            if($value['y'] >= $y){
                if($value['x'] == $x || $value['x'] > ($x - $buffer)){
                    $x = $value['x'];
                    $y = $value['y'];
                    array_pop($sorted);
                    array_push($sorted, $value);   
                } else {
                    $x = $value['x'];
                    $y = $value['y'];
                    array_push($sorted, $value);                       
                }
            }
        }

        $sorted = array_reverse($sorted);

        return $sorted;
    }

    /**
     * Generates datapoints for specific Series, Products, or Configuration
     *
     * @param string $series
     * @param string $product
     * @param string $configuration
     * @return array
     */
    public function get_datapoints($series = null, $product = null, $configuration = null){

        $datapoints_all  = array();
        $datapoints_flat = array();

        $products_data = $_SESSION['linear_le_motor_data'];

        /* datapoints for series */
        if($series != null && $product == null && $configuration == null){

            $products      = $products_data[$series]['products'];
            foreach($products as $product_id => $product_data){
                $torque = $this->torque_calculator($product_data);
                foreach($product_data['attributes']['screw']['options'] as $attr_id => $attr_data){
                    $leadscrew_data = $_SESSION['linear_le_screw_data'][$attr_id];
                    $points         = $this->leadscrew_calculation($torque, $leadscrew_data);
                    array_push($datapoints_all, $points);        
                }
            }
        } 

        /* datapoints for motor */
        elseif($series != null && $product != null && $configuration == null){

            $product_data  = $products_data[$series]['products'][$product];
            $torque        = $this->torque_calculator($product_data);

            foreach($product_data['attributes']['screw']['options'] as $attr_id => $attr_data){
                $leadscrew_data = $_SESSION['linear_le_screw_data'][$attr_id];
                $points         = $this->leadscrew_calculation($torque, $leadscrew_data);
                array_push($datapoints_all, $points);        
            }

        } 

        /* datapoints for leadscrews */
        elseif($series != null && $product != null && $configuration != null){

            $product_data   = $products_data[$series]['products'][$product];
            $torque         = $this->torque_calculator($product_data);

            $leadscrew_data = $_SESSION['linear_le_screw_data'][$configuration];
            $points         = $this->leadscrew_calculation($torque, $leadscrew_data);
            array_push($datapoints_all, $points);
        } 

        /* flattens out all datapoints to one level array */
        if(!empty($datapoints_all)){
            foreach($datapoints_all as $points){
                foreach($points as $point){
                    array_push($datapoints_flat, $point);
                }
            }
        }

        if(!empty($datapoints_flat)){
            $datapoints = $this->sort_high_y($datapoints_flat);

            /** add 0 point */
            if($datapoints[0]['x'] != 0){
                $y = $datapoints[0]['y'];
                $datapoint_zero = ['x' => 0, 'y' => $y * 1.02];
                array_unshift($datapoints, $datapoint_zero);
            }

        } else {
            $datapoints = null;
        }

        return $datapoints;
    }

    public function min_max_amperage($selected_series = '', $selected_product = ''){

        // $selected_series      = $this->parameters->parameter(SERIES);
        // $selected_product     = $this->parameters->parameter(MOTOR);
        $selected_series_data = $_SESSION['linear_le_motor_data'];
        $amperage = array();

        /* amperage for all products */
        if($selected_series == '' && $selected_product == ''){
            foreach($selected_series_data as $series_data){
                if($series_data['series_stock_status'] == 'instock'){
                    foreach($series_data['products'] as $product_data){
                        $amp = $product_data['tested_a'];
                        array_push($amperage, $amp);
                    }                    
                }
            }
        }
        /* amperage for selected series of motors */
        elseif($selected_series != '' && $selected_product == ''){
            $series_data = $selected_series_data[$selected_series];
            foreach($series_data['products'] as $product_data){
                $amp = $product_data['tested_a'];
                array_push($amperage, $amp);
            }            
        }
        /* amperage for only this one product */
        elseif($selected_series != '' && $selected_product != ''){
            $product_data = $selected_series_data[$selected_series]['products'][$selected_product];
            $amp = $product_data['tested_a'];
            array_push($amperage, $amp);
        }

        $min = min($amperage);
        $max = max($amperage);

        $min_max = array('min' => $min, 'max' => $max);

        return $min_max;
    }
    
    public function min_max_amperage_series($series_data){

        $amperage = array();

        foreach($series_data['products'] as $product_data){
            $amp = $product_data['tested_a'];
            array_push($amperage, $amp);
        }

        $min = min($amperage);
        $max = max($amperage);

        $min_max = array('min' => $min, 'max' => $max);

        return $min_max;
    }

    /**
     * Calculate maximum available force at specific speed defined by the user for each set of data. 
     * Finds point A and point B from datapoints. 
     * Uses line equation to find slope and y-intercept to find point of intercept between two lines (line AB and x=speed). 
     *
     * @param array $datapoints
     * @return array 
     */
    public function get_force_at_speed($datapoints){

        $force = param(FORCE);
        $speed = param(SPEED);
        $a = ['x' => 0, 'y' => 0];
        $b = ['x' => 1000, 'y' => 1000];

        /** a = highest point in array that is less than speed
         *  b = lowest point in array that is greater than speed
         */
        foreach($datapoints as $val){
            if($val['x'] < $speed && $val['x'] > $a['x']){
                $a = $val;
            }
            if($val['x'] > $speed && $val['x'] < $b['x']){
                $b = $val;
            }
        }

        /** ensure that point A is greater than 0 before running force calculations. If point A is 0, force at speed is point y and will always work. */
        $slope          = ($b['y'] - $a['y'])/($b['x'] - $a['x']); 
        $y_int          = $a['y'] - ($slope * $a['x']);
        $force_at_speed = ($slope * $speed) + $y_int;
        
        if($force == 0 && $speed == 0){
            $will_work = true;
        } else {
            $will_work      = ($force_at_speed > ($force * 1.1)) ? true : false;  // Add a safety margin of 10%
        }

        // }
        return ['force_at_speed' => $force_at_speed, 'will_work' => $will_work];
    }

}
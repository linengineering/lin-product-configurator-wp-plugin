<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function unit($val, $system, $units){
  if($selected_system == 'metric' && $system == 'imperial'){

      if($type == 'in'){
        $val   = $val * 25.4;
        $system = 'metric';
        $units  = 'mm';
      }
      
      if($type == 'oz-in'){
        $val = $val / 141.611932278059;
        $system = 'metric';
        $units  = 'Nm';
      }
  }
  if($selected_system == 'metric' && $system == 'imperial'){

    if($type == 'mm'){
      $val   = $val / 25.4;
      $system = 'imperial';
      $units  = 'in';
    }
    
    if($type == 'oz-in'){
      $val = $val * 141.611932278059;
      $system = 'imperial';
      $units  = 'Nm';
    }

  }
  return array($val, $system, $type); 
}
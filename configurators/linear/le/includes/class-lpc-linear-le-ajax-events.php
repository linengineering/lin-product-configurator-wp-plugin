<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Handles main ajax events for Linear LE configurator
 */
class LPC_linear_le_ajax_events extends LPC_ajax_handler{

    public $ajax_events   = array(
      'chart_calc_button'         => true,
      'get_chart_data'            => true,
      'dimension'                 => true,
      'product_specifications'    => true,
      'get_price'                 => true,
      'get_lead_time'             => true,
      'add_to_cart'               => true,
      'screw_end_finish'          => true,
      'nut_dimensions'            => true,
      'get_main_linear_le'        => true,
      'get_elements'              => true,
    );

    static function init(){
      $handler = new self;
      $handler->register_events();
      $handler->localize_scripts();
    }

    public function scripts_to_localize(){
      //wp_localize_script( 'lpc-linear-le-ajax', 'lpc_linear_le_ajax_data', $this->get_ajax_data() );
    }

    /**
     * Return specific elements methods
     */

    public function element_price(){
      $class = new LPC_linear_le_price_leadtime;
      return $class->get_price();
    }

    public function element_leadtime(){
      $class = new LPC_linear_le_price_leadtime;
      return $class->get_lead_time();
    }

    public function element_charts(){
      $chart = new Lpc_linear_le_charts();
      return $charts->datasets();
    }

    public function element_motor_dimensions(){
      $dimensions = new LPC_linear_le_product_diagram_container();
      return $dimensions->inner_container();
    }

    public function element_motor_specifications(){
      $specificaton_section = new LPC_linear_le_product_spec_container();
      return $specificaton_section->inner_container();
    }

    public function element_end_finish_dimensions(){
      $specificaton_section = new LPC_linear_le_product_leadscrew_modifications_container();
      return $specificaton_section->inner_container();
    }

    public function element_nut_dimensions(){
      $nut_container = new LPC_linear_le_product_nut_options_container;
      return $nut_container->inner_container();      
    }

    public function data_charts(){
      return $_SESSION['linear_le_chart_data'];
    }

    public function data_motors(){
      return $_SESSION['linear_le_motor_data'];
    }

    public function data_screws(){
      return $_SESSION['linear_le_screw_data'];
    }

    public function data_end_finish(){
      return $_SESSION['linear_le_end_finish_data'];
    }

    public function data_nuts(){
      return $_SESSION['linear_le_nut_data'];
    }

    public function chart_calc_button(){
      check_ajax_referer(self::NONCE);
      $chart = new Lpc_linear_le_charts();
      $datasets = $charts->datasets();
      wp_send_json($datasets);
      die();
    }

    public function get_chart_data(){
      check_ajax_referer(self::NONCE);
      $chart = new Lpc_linear_le_charts();
      $chart_data = $charts->chart_datasets();
      wp_send_json($chart_data);
      die();
    }

    public function dimension(){
      check_ajax_referer(self::NONCE);
      $dimensions = new LPC_linear_le_product_diagram_container();
      $html = $dimensions->inner_container();
      wp_send_json($html);
      die();
    }

    public function product_specifications(){
      check_ajax_referer(self::NONCE);
      $specificaton_section = new LPC_linear_le_product_spec_container();
      $html = $specificaton_section->inner_container();
      wp_send_json($html);
      die();
    }

    public function get_price(){
      check_ajax_referer(self::NONCE);
      $class = new LPC_linear_le_price_leadtime;
      wp_send_json($class->get_price());
      die();
    }

    public function get_lead_time(){
      check_ajax_referer(self::NONCE);
      $class = new LPC_linear_le_price_leadtime;
      wp_send_json($class->get_lead_time());
      die();
    }

    public function add_to_cart(){
      check_ajax_referer(self::NONCE);
      $query_string = filter_var($_SERVER['QUERY_STRING'], FILTER_SANITIZE_URL);
      $_SESSION['item_permalink'] = '/rapid-prototype-configurator?' . $query_string;
      $cart_class = new LPC_linear_le_step_review_template;
      $cart_class->add_to_cart();
      wp_send_json($cart_class->cart_message());
      die();
    }

    public function screw_end_finish(){
      check_ajax_referer(self::NONCE);
      $screw_end_finish = new LPC_linear_le_product_leadscrew_modifications_container;
      $html = $screw_end_finish->inner_container();
      wp_send_json($html);
      die();
    }

    public function nut_dimensions(){
      check_ajax_referer(self::NONCE);
      $nut_container = new LPC_linear_le_product_nut_options_container;
      $html = $nut_container->inner_container();
      wp_send_json($html);
      die();
    }

    public function get_main_linear_le(){
      check_ajax_referer(self::NONCE);
      
      $templates  = new LPC_templates;
      $html       = $templates->main_content();

      $charts     = new Lpc_linear_le_charts;
      $chart_data = $charts->chart_datasets();

      wp_send_json(['HTML' => $html, 'DATA' => $chart_data]);
      die();
    }

    public function get_elements(){
      check_ajax_referer(self::NONCE);

      $main_ajax_class = new LPC_ajax_events;

      wp_send_json([
        'element_price'                 => $this->element_price(),
        'element_leadtime'              => $this->element_leadtime(),
        'element_motor_dimensions'      => $this->element_motor_dimensions(),
        'element_motor_specifications'  => $this->element_motor_specifications(),
        'element_end_finish_dimensions' => $this->element_end_finish_dimensions(),
        'element_nut_dimensions'        => $this->element_nut_dimensions(),
        'element_nav'                   => $main_ajax_class->element_nav(),
      ]);

      die();
    }

}
LPC_linear_le_ajax_events::init();

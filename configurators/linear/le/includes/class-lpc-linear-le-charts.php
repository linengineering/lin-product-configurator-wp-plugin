<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Lpc_linear_le_charts {

    public $parameters;
    public $products;
    public $calculations;
    public $colors;

    public function __construct(){
        $this->parameters         = new LPC_parameters();
        $this->products           = new LPC_linear_le_products();
        $this->calculations       = new LPC_linear_le_calculations();
        $this->colors             = new LPC_colors();
    }

    public function set_chart_data_session(){
        $_SESSION['linear_le_chart_data'] = $this->get_charts_data();
    }

    /**
     * Generate all chart datapoints for all configuration
     *
     * @return array charts data
     */
    public function get_charts_data(){

        $chart_data         = array();
        $motor_session_data = (isset($_SESSION['linear_le_motor_data'])) ? $_SESSION['linear_le_motor_data'] : null;

        $c_s = 0; // Series color code
        if($motor_session_data != null){
            
            $chart_data['title']      = 'All Available Configurations';
            $chart_data['subtitle']   = $this->chart_subtitle('', '');
            
            /** series datapoints */
            foreach($motor_session_data as $series_id => $series_data){
                if($series_data['series_stock_status'] == 'instock'){
                    $series_datapoints = $this->calculations->get_datapoints($series_id);
                    $force_at_speed    = $this->calculations->get_force_at_speed($series_datapoints);
                    $chart_data['series'][$series_id]['datapoints'] = $series_datapoints;
                    $chart_data['series'][$series_id]['force']      = $force_at_speed;
                    $chart_data['series'][$series_id]['color']      = $this->colors->color[$c_s];
                    $chart_data['series'][$series_id]['title']      = sprintf('All %s Series Configurations', $_SESSION['linear_le_motor_data'][$series_id]['series_name']);
                    $chart_data['series'][$series_id]['subtitle']   = $this->chart_subtitle($series_id, '');
                    
                    $c_s++;
                    $c_m = 0; // motor color code 

                    /** motor datapoints */
                    foreach($series_data['products'] as $motor_id => $motor_data){
                        if($motor_data['stock_status'] == 'instock'){
                            $motor_datapoints = $this->calculations->get_datapoints($series_id, $motor_id);
                            $force_at_speed    = $this->calculations->get_force_at_speed($motor_datapoints);
                            $chart_data['series'][$series_id]['motors'][$motor_id]['datapoints'] = $motor_datapoints;
                            $chart_data['series'][$series_id]['motors'][$motor_id]['force']      = $force_at_speed;
                            $chart_data['series'][$series_id]['motors'][$motor_id]['color']      = $this->colors->color[$c_m];
                            $chart_data['series'][$series_id]['motors'][$motor_id]['title']      = sprintf('All %s Configurations', $_SESSION['linear_le_motor_data'][$series_id]['products'][$motor_id]['name']);
                            $chart_data['series'][$series_id]['motors'][$motor_id]['subtitle']   = $this->chart_subtitle($series_id, $motor_id);

                            $c_m++;
                            $c_l = 0; // screw color code

                            /** screw datapoints */
                            foreach($motor_data['attributes']['screw']['options'] as $screw_id => $screw_data){
                                $screw_datapoints = $this->calculations->get_datapoints($series_id, $motor_id, $screw_id);
                                $force_at_speed    = $this->calculations->get_force_at_speed($screw_datapoints);
                                if($_SESSION['linear_le_screw_data'][$screw_id]['stock'] > 0){
                                    $chart_data['series'][$series_id]['motors'][$motor_id]['screws'][$screw_id]['datapoints'] = $screw_datapoints;  
                                    $chart_data['series'][$series_id]['motors'][$motor_id]['screws'][$screw_id]['force']      = $force_at_speed;
                                    $chart_data['series'][$series_id]['motors'][$motor_id]['screws'][$screw_id]['color']      = $this->colors->color[$c_l];
                                    $chart_data['series'][$series_id]['motors'][$motor_id]['screws'][$screw_id]['title']      = sprintf('%s with %s screw', $_SESSION['linear_le_motor_data'][$series_id]['products'][$motor_id]['name'], $_SESSION['linear_le_screw_data'][$screw_id]['name']);
                                    $chart_data['series'][$series_id]['motors'][$motor_id]['screws'][$screw_id]['subtitle']   = $this->chart_subtitle($series_id, $motor_id);

                                    $c_l++;                                    
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return $chart_data;
    }

    /**
     * Tempalte
     *
     */
    public function template(){

        $min_max = $this->calculations->min_max_amperage();

        $force    = ( empty(param(FORCE)) )    ? 0  : param(FORCE); 
        $speed    = ( empty(param(SPEED)) )    ? 0  : param(SPEED); 
        $voltage  = ( empty(param(VOLTAGE)) )  ? 24 : param(VOLTAGE); 
        $amperage = ( empty(param(AMPERAGE)) ) ? $min_max['max'] : param(AMPERAGE); 
        
        ob_start();
        ?>
        <div class="user-input-form" data-scrollTo='tooltip' tooltipPosition="top" data-step="1" data-intro="Enter Your Speed and Force requirements and your available voltage and amperage (your driver output) and hit calculate.">

            <div class="input-section">

                <header class="inputs-header">
                    <span>Required Force/Speed:</span>
                </header>            

                <div class="input-container">
                    <span class="input-label">
                        Force (lbs)<a> <i class="fas fa-question-circle"></i></a>
                    </span>      
                    <div class="input-input">
                        <input type="number" step="any" id="force" name="force" value="<?php echo $force; ?>">
                    </div>                
                    <div class="input-slider">
                        <div class="slider" data-slider data-initial-start="<?php echo $force; ?>" data-start="0" data-end="150" data-step="0.25" data-decimal="3" data-position-value-function="pow" data-non-linear-base="5">
                            <span class="slider-handle" data-slider-handle role="slider" tabindex="1" aria-controls="force"></span>
                            <span class="slider-fill" data-slider-fill></span>
                        </div>
                    </div>
                </div>

                
                <div class="input-container">
                    <span class="input-label">
                        Speed (in/sec)<a> <i class="fas fa-question-circle"></i></a>
                    </span>      
                    <div class="input-input">
                        <input type="number" step="any" id="speed" name="speed" value="<?php echo $speed; ?>">
                    </div>                
                    <div class="input-slider">
                        <div class="slider" data-slider data-initial-start="<?php echo $speed; ?>" data-start="0" data-end="16" data-step="0.1" data-decimal="3" data-position-value-function="pow" data-non-linear-base="5">
                            <span class="slider-handle" data-slider-handle role="slider" tabindex="1" aria-controls="speed"></span>
                            <span class="slider-fill" data-slider-fill></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="input-section">

                <header class="inputs-header">
                    <span>Available Power:</span>
                </header>

                <div class="input-container">
                    <span class="input-label">
                        Voltage (VDC)<a> <i class="fas fa-question-circle"></i></a>
                    </span>      
                    <div class="input-input">
                        <input type="number" step="any" id="voltage" name="voltage" value="<?php echo $voltage; ?>">
                    </div>                
                    <div class="input-slider">
                        <div class="slider" data-slider data-initial-start="<?php echo $voltage; ?>" data-start="5" data-end="48" data-step="1" data-decimal="3">
                            <span class="slider-handle" data-slider-handle role="slider" tabindex="1" aria-controls="voltage"></span>
                            <span class="slider-fill" data-slider-fill></span>
                        </div>
                    </div>
                </div>

                
                <div class="input-container">
                    <span class="input-label">
                        Amperage (A)<a> <i class="fas fa-question-circle"></i></a>
                    </span>      
                    <div class="input-input">
                        <input type="number" step="any" id="amperage" name="amperage" value="<?php echo $amperage; ?>">
                    </div>                
                    <div class="input-slider">
                        <div class="slider" data-slider data-initial-start="<?php echo $amperage; ?>" data-start="0.1" data-end="3" data-step="0.1" data-decimal="3">
                            <span class="slider-handle" data-slider-handle role="slider" tabindex="1" aria-controls="amperage"></span>
                            <span class="slider-fill" data-slider-fill></span>
                        </div>
                    </div>
                </div>

            </div>

            <button id="chart_calc_button" class="button expanded round">Calculate</button>

        </div>

        <div class="configurator-chart" id="force-speed-chart" data-step="2" data-scrollTo='tooltip' data-intro="Available configurations based on your requirements will be ploted in the chart to help you make your selections. Simply choose a motor or configuration that is within your required operating range">
            <div class="chart-content" id="lpc-chart"></div>
        </div>

        <?php 
        return ob_get_clean();
    }

    /**
     * Generate datasets for chart
     *
     * @return array dataset 
     */
    public function chart_datasets(){
        
        $datasets       = null;

        $step           = param(STEP);
        $substep        = param(SUBSTEP);
        $product_series = param(SERIES);
        $base_pn        = param(MOTOR);
        $screw_type     = param(SCREW);

        if($step == 'motor' && $substep == 'series'){
            if($product_series == ''){
                $datasets = $this->chart_datasets_all_series();
            } else {
                $datasets = $this->chart_datasets_single_series();
            }
        }

        if($step == 'motor' && $substep == 'length'){
            if($base_pn == ''){
                $datasets = $this->chart_datasets_all_products();
            } else {
                $datasets = $this->chart_datasets_single_product();
            }
        }

        if($step == 'leadscrew' && $substep == 'options'){
            if($screw_type == ''){
                $datasets = $this->chart_datasets_all_leadscrews();
            } else {
                $datasets = $this->chart_datasets_single_leadscrew();
            }
        }

        if($step == 'leadscrew' && $substep == 'modifications'){
            $datasets = $this->chart_datasets_single_leadscrew();
        }

        if($step == 'nut' && $substep == 'options'){
            $datasets = $this->chart_datasets_single_leadscrew();
        }

        if($step == 'review' && $substep == 'configuration'){
            $datasets = $this->chart_datasets_single_leadscrew();
        }
        return $datasets;
    }





    public function chart_subtitle($series, $motor){

        $selected_voltage   = (float)param(VOLTAGE);
        $selected_amperage  = (float)param(AMPERAGE);

        $min_max = $this->calculations->min_max_amperage($series, $motor);

        $display_voltage = ($selected_voltage == '') ? 24 : $selected_voltage;
        $display_amperage = ($selected_amperage == '') ? $min_max['max'] : $selected_amperage;

        if($selected_amperage <= $min_max['min']){
            $subtitle = $display_voltage . ' VDC, ' . $display_amperage . ' Amp/Phase'; 
        }
        elseif($selected_amperage >= $min_max['min'] && $selected_amperage <= $min_max['max']){
            $subtitle = $display_voltage . ' VDC, ' . $min_max['min'] . ' - ' . $display_amperage . ' Amp/Phase'; 
        }
        elseif($selected_amperage >= $min_max['max']){
            $subtitle = $display_voltage . ' VDC, ' . $min_max['min'] . ' - ' . $min_max['max'] . ' Amp/Phase'; 
        }
        
        if($min_max['min'] == $min_max['max'] && $selected_amperage <= $min_max['min']) {
            $subtitle = $display_voltage . ' VDC, ' . $display_amperage . ' Amp/Phase'; 
        } 
        elseif($min_max['min'] == $min_max['max'] && $display_amperage >= $min_max['max']){
            $subtitle = $display_voltage . ' VDC, ' . $min_max['max'] . ' Amp/Phase'; 
        }

        return $subtitle;
        
    }

}
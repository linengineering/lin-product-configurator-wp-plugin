<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_linear_le_products {

    /**
     * products object
     *
     * @var object
     */         
    public $products;

    public function __construct(){
        $this->products = new LPC_products('le');
    }

    public function set_products_data_session(){
        $_SESSION['linear_le_motor_data']            = $this->get_products_data();
        $_SESSION['linear_le_screw_data']            = $this->get_screw_data();
        $_SESSION['linear_le_screw_end_finish_data'] = $this->get_screw_end_finish_data();
        $_SESSION['linear_le_nut_data']              = $this->get_nut_data();
    }

    public function set_value($arr, $value, $default = ''){
        if(isset($arr[$value][0])){
            return $arr[$value][0];
        } else {
            return $default;
        }
    }
    /**
     * Generate product data array for all products associated with this category and type
     *
     * @return array
     */
    public function get_products_data(){

        $data = $this->products->get_series_data();

        foreach($data as $series_id => $series_data){

            $series_stock_status = array();

            foreach($series_data['products'] as $product_id => $product_values){

                /* get woocommerce data for this product */
                $product_wc_data = wc_get_product($product_id)->get_data();

                /* get wp post meta data for this product. For all custom fields in woocommerce product manager */
                $product_wp_data = get_post_meta($product_id);

                /* check for motor inventory, at least one motor needs to be in stock for the series to be considered 'in stock' */
                $stock_status = $product_wc_data['stock_status'];

                if(!in_array($stock_status, $series_stock_status)){
                    array_push($series_stock_status, $stock_status);
                }                
                
                /* Series level data */
                $data[$series_id]['term_id']             = $series_id;
                $data[$series_id]['step_angle']          = $this->set_value($product_wp_data, '_step_angle');
                $data[$series_id]['nema_size']           = $this->set_value($product_wp_data, '_nema_size');
                $data[$series_id]['frame_size']          = $this->set_value($product_wp_data, '_frame_size');
                $data[$series_id]['price']               = $this->set_value($product_wp_data, '_price');
                $data[$series_id]['lead_time']           = $this->set_value($product_wp_data, '_lead_time');
                $data[$series_id]['series_stock_status'] = (in_array('instock', $series_stock_status)) ? 'instock' : 'outofstock';

                /* Motor level data */
                $data[$series_id]['products'][$product_id] = array(); // set product as 
                $data[$series_id]['products'][$product_id]['term_id']            = $product_id;
                $data[$series_id]['products'][$product_id]['step_angle']         = $this->set_value($product_wp_data, '_step_angle');
                $data[$series_id]['products'][$product_id]['nema_size']          = $this->set_value($product_wp_data, '_nema_size');
                $data[$series_id]['products'][$product_id]['frame_size']         = $this->set_value($product_wp_data, '_frame_size');
                $data[$series_id]['products'][$product_id]['name']               = $this->set_value($product_wp_data, '_sku'); 
                $data[$series_id]['products'][$product_id]['stock_status']       = $this->set_value($product_wp_data, '_stock_status');
                $data[$series_id]['products'][$product_id]['price']              = $this->set_value($product_wp_data, '_price');
                $data[$series_id]['products'][$product_id]['availability']       = $this->set_value($product_wp_data, '_availability');
                $data[$series_id]['products'][$product_id]['holding_torque']     = $this->set_value($product_wp_data, '_holding_torque');
                $data[$series_id]['products'][$product_id]['torque_array']       = (isset($product_wp_data['_torque_array'][0])) ? str_replace([' ', PHP_EOL], '', $product_wp_data['_torque_array'][0]) : 0; 
                $data[$series_id]['products'][$product_id]['tested_a']           = $this->set_value($product_wp_data, '_tested_a');
                $data[$series_id]['products'][$product_id]['tested_v']           = $this->set_value($product_wp_data, '_tested_v');
                $data[$series_id]['products'][$product_id]['dimension_a']        = $this->set_value($product_wp_data, '_dimension_a');
                $data[$series_id]['products'][$product_id]['tested_stepping']    = $this->set_value($product_wp_data, '_tested_stepping');
                $data[$series_id]['products'][$product_id]['current']            = $this->set_value($product_wp_data, '_current');
                $data[$series_id]['products'][$product_id]['resistance']         = $this->set_value($product_wp_data, '_resistance');
                $data[$series_id]['products'][$product_id]['power_draw']         = $this->set_value($product_wp_data, '_power_draw');
                $data[$series_id]['products'][$product_id]['winding']            = $this->set_value($product_wp_data, '_winding');
                $data[$series_id]['products'][$product_id]['inertia']            = $this->set_value($product_wp_data, '_inertia');
                $data[$series_id]['products'][$product_id]['weight']             = $this->set_value($product_wp_data, '_weight');
                $data[$series_id]['products'][$product_id]['no_of_leads']        = $this->set_value($product_wp_data, '_no_of_leads');
                $data[$series_id]['products'][$product_id]['lead_time']          = $this->set_value($product_wp_data, '_lead_time');
                $data[$series_id]['products'][$product_id]['availablity']        = $this->set_value($product_wp_data, '_availablity');
                $data[$series_id]['products'][$product_id]['sap_number']         = $this->set_value($product_wp_data, '_sap_number');
                
                /* Attribute level data */
                foreach($product_wc_data['attributes'] as $tax_name => $attribute_values){
                    
                    $tax_name_short = str_replace('pa_linear-le-', '', $tax_name);
                    $attribute_data = $attribute_values['data'];

                    $data[$series_id]['products'][$product_id]['attributes'][$tax_name_short]['term_id']        = $attribute_data['id'];
                    $data[$series_id]['products'][$product_id]['attributes'][$tax_name_short]['tax_name_full']  = $attribute_data['name'];
                    $data[$series_id]['products'][$product_id]['attributes'][$tax_name_short]['tax_name_short'] = $tax_name_short;

                    foreach($attribute_data['options'] as $option_id){
                        $data[$series_id]['products'][$product_id]['attributes'][$tax_name_short]['options'][$option_id]['term_id'] = $option_id;
                    }
                }

                /* Diagram Images */
                $dimension_imgs = $product_wc_data['gallery_image_ids'];
                
                if(!empty($dimension_imgs)){
                    if(isset($dimension_imgs[0])){
                        $data[$series_id]['dim_front_img_id'] = $dimension_imgs[0];
                        $data[$series_id]['products'][$product_id]['dim_front_img_id'] = $dimension_imgs[0];
                    }
                    if(isset($dimension_imgs[1])){
                        $data[$series_id]['dim_side_img_id'] = $dimension_imgs[1];
                        $data[$series_id]['products'][$product_id]['dim_side_img_id'] = $dimension_imgs[1];                        
                    }
                } else {
                    $data[$series_id]['dim_front_img_id'] = 173;
                    $data[$series_id]['dim_side_img_id'] = 173;
                    $data[$series_id]['products'][$product_id]['dim_front_img_id'] = 173;   
                    $data[$series_id]['products'][$product_id]['dim_side_img_id'] = 173; 
                }
            }
        }
        return $data;
    }

    public function get_screw_data(){
        $items = get_terms(['taxonomy' => 'pa_linear-le-screw', 'hide_empty' => false]);
        $data  = array();
        foreach($items as $item){
            $id = $item->term_id;
            $data[$id] = $this->get_screw_attributes($id);
        }
        return $data;
    }

    public function get_screw_attributes($id){
        
        $meta = get_term_meta($id);
        $term = get_term($id);
        $data = array();

        $data['id']         = $id;
        $data['name']       = (isset($meta['_name'][0])) ? $meta['_name'][0] : '';
        $data['slug']       = (isset($term->slug)) ? $term->slug : '';
        $data['parent_tax'] = (isset($term->taxonomy)) ? $term->taxonomy : '';
        $data['desc']       = (isset($meta['_desc'][0])) ? $meta['_desc'][0] : 1;
        $data['price']      = (isset($meta['_price'][0])) ? $meta['_price'][0] : 1;
        $data['stock']      = (isset($meta['_stock'][0])) ? $meta['_stock'][0] : 1;
        $data['in_stock']   = (isset($meta['_in_stock'][0])) ? $meta['_in_stock'][0] : 0;
        $data['lead_time']  = (isset($meta['_lead_time'][0])) ? $meta['_lead_time'][0] : 0;

        $data['nut_attributes']     = (isset($meta['_nut_attributes'][0])) ? $meta['_nut_attributes'][0] : 0;
        $data['finish_attributes']  = (isset($meta['_finish_attributes'][0])) ? $meta['_finish_attributes'][0] : 0;

        $data['od']         = (isset($meta['_od'][0])) ? $meta['_od'][0] : 1;
        $data['root_od']    = (isset($meta['_root_od'][0])) ? $meta['_root_od'][0] : 1;
        $data['lead']       = (isset($meta['_lead'][0])) ? $meta['_lead'][0] : 1;
        $data['img_id']     = (isset($meta['_img_id'][0])) ? $meta['_img_id'][0] : 173;

        return $data; 
    }

    public function get_nut_data(){
        $items = get_terms(['taxonomy' => 'pa_linear-le-nut', 'hide_empty' => false]);
        $data  = array();
        foreach($items as $item){
            $id = $item->term_id;
            $data[$id] = $this->get_nut_attributes($id);
        }
        return $data;
    }

    public function get_nut_attributes($id){

        $meta = get_term_meta($id);
        $term = get_term($id);
        $data = array();

        $data['id']          = $id;
        $data['name']        = (isset($meta['_name'][0])) ? $meta['_name'][0] : '';
        $data['slug']        = (isset($term->slug)) ? $term->slug : '';
        $data['parent_tax']  = (isset($term->taxonomy)) ? $term->taxonomy : '';
        $data['desc']        = (isset($meta['_desc'][0])) ? $meta['_desc'][0] : 1;
        $data['price']       = (isset($meta['_price'][0])) ? $meta['_price'][0] : 1;
        $data['lead_time']   = (isset($meta['_lead_time'][0])) ? $meta['_lead_time'][0] : 1;
        $data['efficiency']  = (isset($meta['_efficiency'][0])) ? $meta['_efficiency'][0] : 1;
        $data['max_force']   = (isset($meta['_max_force'][0])) ? $meta['_max_force'][0] : 1;
        $data['img_id']      = (isset($meta['_img_id'][0])) ? $meta['_img_id'][0] : 0;
        $data['dim_img_id']  = (isset($meta['_dim_img_id'][0])) ? $meta['_dim_img_id'][0] : 0;
        $data['nut_style']   = (isset($meta['_nut_style'][0])) ? $meta['_nut_style'][0] : 0;
        $data['flange_type'] = (isset($meta['_flange_type'][0])) ? $meta['_flange_type'][0] : 0;
        $data['od']          = (isset($meta['_od'][0])) ? $meta['_od'][0] : 0;
        $data['length']      = (isset($meta['_length'][0])) ? $meta['_length'][0] : 0;

        return $data; 
    }

    public function get_screw_end_finish_data(){
        $items = get_terms(['taxonomy' => 'pa_linear-le-screw-end-finish', 'hide_empty' => false,]);
        $data  = array();
        foreach($items as $item){
            $id = $item->term_id;
            $data[$id] = $this->get_screw_end_finish_attributes($id);
        }
        return $data;
    }

    public function get_screw_end_finish_attributes($id){

        $meta = get_term_meta($id);
        $term = get_term($id);
        $data = array();

        $data['id']          = $id;
        $data['name']        = (isset($meta['_name'][0])) ? $meta['_name'][0] : '';
        $data['slug']        = (isset($term->slug)) ? $term->slug : '';
        $data['parent_tax']  = (isset($term->taxonomy)) ? $term->taxonomy : '';
        $data['price']       = (isset($meta['_price'][0])) ? $meta['_price'][0] : 1;
        $data['lead_time']   = (isset($meta['_lead_time'][0])) ? $meta['_lead_time'][0] : 1;
        $data['img_id']      = (isset($meta['_img_id'][0])) ? $meta['_img_id'][0] : 1;

        $data['dim_u']       = (isset($meta['_dim_u'][0])) ? $meta['_dim_u'][0] : 1;
        $data['dim_v']       = (isset($meta['_dim_v'][0])) ? $meta['_dim_v'][0] : 1;
        $data['dim_t']       = (isset($meta['_dim_t'][0])) ? $meta['_dim_t'][0] : 1;
        $data['dim_x']       = (isset($meta['_dim_x'][0])) ? $meta['_dim_x'][0] : 1;
        $data['dim_y']       = (isset($meta['_dim_y'][0])) ? $meta['_dim_y'][0] : 1;
        
        return $data; 
    }

}
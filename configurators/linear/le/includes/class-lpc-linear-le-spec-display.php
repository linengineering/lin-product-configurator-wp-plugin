<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_linear_spec_display {

    public function nema_size($val){
        $nema_size = 'NEMA ' . $val;
        return $nema_size;
    }

    public function frame_size($val){
        $val = sprintf('%0.2f', (float)$val);
        $frame_size = sprintf('%0.2f', $val / 25.4) . '"' . ' (' . $val . 'mm)';
        return $frame_size;
    }

    public function step_angle($val){
        $step_angle = $val . '&deg;' . ' (' . 360 / $val . ' steps/rev)';
        return $step_angle;
    }

    public function amp($val){
        $amp = $val . ' Amp/Phase';
        return $amp;
    }

    public function amp_range($val_1, $val_2){
        if($val_1 == $val_2){
            $amp_range = $val_2 . ' Amp/Phase';
        } else {
            $amp_range = $val_1 . ' to ' . $val_2 . ' Amp/Phase';
        }
        
        return $amp_range;
    }

    public function motor_length($val){
        $motor_length = sprintf('%0.2f', $val / 25.4) . '" ' . ' (' . $val . 'mm)';
        return $motor_length;
    }

    public function motor_length_range($val_1, $val_2){
        if($val_1 == $val_2){
           $motor_length_range = sprintf('%0.2f', $val_2 / 25.4) . '" ' . ' (' . $val_2 . ' mm)';
        } else {
           $motor_length_range = sprintf('%0.2f', $val_1 / 25.4) . ' to ' . sprintf('%0.2f', $val_2 / 25.4) . ' in (' . $val_1 . ' to ' . $val_2 . ' mm)'; 
        }
        return $motor_length_range;
    }

    public function holding_torque($val){
        $holding_torque = sprintf('%0.2f', $val * 141.61193227806 ) . ' oz-in' . ' (' .$val. ' Nm)';
        return $holding_torque;
    }

    public function resistance($val){
        $resistance = $val . ' Ohm/Phase';
        return $resistance;
    }

    public function power_draw($val){
        $power_draw = $val . ' W';
        return $power_draw;
    }

    public function inertia($val){
        $inertia = sprintf('%0.2f', $val * 0.005467475 ) . ' oz-in&sup2;' . ' (' .$val. ' g-cm&sup2;)';
        return $inertia;
    }

    public function weight($val){
        $weight = $val . ' oz';
        return $weight;
    }

    public function leadscrew_od($val){
        $leadscrew_od = sprintf('%0.3f', $val / 25.4) . '"' . ' (' .$val. ' mm)';
        return $leadscrew_od;
    }

    public function leadscrew_lead($val){
        $leadscrew_lead = sprintf('%0.3f', $val / 25.4) . '"' . ' (' .$val. ' mm)';
        return $leadscrew_lead;
    }

    public function leadscrew_length($val){
        $leadscrew_length = $val . 'mm' . ' (' .sprintf('%0.2f', $val / 25.4). '")';
        return $leadscrew_length;
    }

    public function end_finish_dim($val){
        $end_finish_dim = sprintf('%0.4f', $val / 25.4) . '"' . ' (' .sprintf('%0.3f', $val). ' mm)';
        return $end_finish_dim;
    }
}
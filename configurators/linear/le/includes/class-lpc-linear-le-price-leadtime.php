<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_linear_le_price_leadtime {

    public function __construct(){
        $this->products_class = new LPC_linear_le_products;
    }

    public function get_price(){
        $products = $_SESSION['linear_le_motor_data'];
        $price = 0;
        if(!empty(param(SERIES))){
            $series = $products[param(SERIES)];
            $price += $series['price'];
        }
        if(!empty(param(SCREW_LENGTH))){
            $length = (int)param(SCREW_LENGTH);
            $add    = 15;
            if($length < 300){
                $price += $add;
            }
        }
        if(!empty(param(SCREW_FINISH))){
            $finish = $_SESSION['linear_le_screw_end_finish_data'][param(SCREW_FINISH)];
            $price += $finish['price'];
        }
        return $price;
    }

    public function get_lead_time(){
        $products = $_SESSION['linear_le_motor_data'];
        $base = 0; 
        $lead = 0;
        if(!empty(param(SERIES))){
            $series = $products[param(SERIES)];
            $base += $series['lead_time'];
            $lead += $series['lead_time'];
        }
        if(!empty(param(SCREW_LENGTH))){
            $length = (int)param(SCREW_LENGTH);
            $time   = 2; 
            if($length < 300){
                if($base + $time > $lead){
                    $lead = $base + $time;
                }                
            }
        }
        if(!empty(param(SCREW_FINISH))){
            $finish = $_SESSION['linear_le_screw_end_finish_data'][param(SCREW_FINISH)];
            if($base + $finish['lead_time'] > $lead){
                $lead = $base + $finish['lead_time'];
            }
        }
        return $lead;
    }
}
<?php 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_linear_le_steps_and_parameters {

   /**
   * Define Configurator Steps;
   * @var array
   */
    public $steps = array(
        'motor' => array(
            'step'          => 1,
            'name'          => 'Motor',
            'desc'          => 'Configure Motor',
            'substeps'      => array(
                'series'   => array(
                    'substep'   => 1,
                    'name'      => 'Motor Series',
                    'desc'      => 'Choose the series of motor that best fits your requirements',
                ),
                'length'    => array(
                    'substep'   => 2,
                    'name'      => 'Motor Length',
                    'desc'      => 'Choose the Winding and Length of your motor',
                ),
            ),
        ),
        'leadscrew' => array(
            'step'          => 2,
            'name'          => 'Lead Screw',
            'desc'          => 'Choose a motor that fits your requirements',
            'substeps'      => array(
                'options'   => array(
                    'substep'   => 1,
                    'name'      => 'Options',
                    'desc'      => 'Choose your leadscrew',
                ),
                'modifications'    => array(
                    'substep'   => 2,
                    'name'      => 'Modifications',
                    'desc'      => 'Modify length and end-finish',
                ),
            ),
        ),
        'nut' => array(
            'step'          => 3,
            'name'          => 'Nut',
            'desc'          => 'Choose a nut that best fits your requirement',
            'substeps'      => array(
                'options'   => array(
                    'substep'   => 1,
                    'name'      => 'Options',
                    'desc'      => 'Choose your nut options',
                ),
            ),
        ),
        'review' => array(
            'step'          => 4,
            'name'          => 'Review',
            'desc'          => 'Review Selected Parts',
            'substeps'      => array(
                'configuration' => array(
                    'substep'   => 1,
                    'name'      => 'Review Your Configuration',
                    'desc'      => 'Review Selected Products and Components',
                ),
            ),
        )
    );

    /**
     * Define parameters for this configurator [KEY] => [DEFAULT VALUE];
     * @var array
     */
    public $parameters = array();

    public function __construct(){
        $this->param_methods = new LPC_parameters();
        $this->define_parameters();
        $this->set_parameters();
    }

    /**
     * Define query string parameters
     *
     * @return void
     */
    public function define_parameters(){
        if(!defined('SERIES')){
            define('FORCE', 'i1');
            define('SPEED', 'i2');
            define('VOLTAGE', 'i3');
            define('AMPERAGE', 'i4');
            define('SERIES', 'p1');
            define('MOTOR', 'p2');
            define('SCREW', 'p3');
            define('SCREW_LENGTH', 'p4');
            define('SCREW_COATING', 'p5');
            define('SCREW_FINISH', 'p6');
            define('NUT', 'p7');
            define('COLOR_SERIES', 'p8');
            define('COLOR_MOTOR', 'p9');
            define('COLOR_SCREW', 'p10');
        }
    }

    /**
     * Set default values for each parameter 
     *
     * @return void
     */
    public function set_parameters(){
        $this->parameters = array(
            CATEGORY          => 'linear',    //Required
            TYPE              => 'le',        //Required
            STEP              => 'motor',     //Required
            SUBSTEP           => 'series',    //Required
            FORCE             => 0,
            SPEED             => 0,
            VOLTAGE           => 0,
            AMPERAGE          => 0,
            SERIES            => '',
            MOTOR             => '',
            SCREW             => '',
            SCREW_LENGTH      => 0,
            SCREW_COATING     => '',
            SCREW_FINISH      => '',
            NUT               => '',
            COLOR_SERIES      => 0,
            COLOR_MOTOR       => 0,
            COLOR_SCREW       => 0,
        );
    }
}
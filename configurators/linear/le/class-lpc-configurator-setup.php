<?php

namespace linear\le;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class LPC_configurator_setup {

  public $steps;
  public $parameters;

  public static function register_ajax_events(){
    $handler = new self();
    $handler->definitions();
    include_once LPC_LINEAR_LE_DIR . 'includes/class-lpc-linear-le-ajax-events.php';
  }

  public function __construct(){
    $this->definitions();
    $this->steps_and_parameters();
    $this->includes();
    $this->enqueue_scripts();
  }

  public function definitions(){
    if(!defined('LPC_LINEAR_LE_DIR')){
      define('LPC_LINEAR_LE_DIR', LPC_ABSPATH . 'configurators/linear/le/');
    }
    if(!defined('LPC_LINEAR_LE_URI')){
      define('LPC_LINEAR_LE_URI', plugin_dir_url(LPC_ABSPATH . 'configurators/linear/le') . 'le/'); 
    }
  }
  
  public function steps_and_parameters(){

    include_once LPC_LINEAR_LE_DIR . 'includes/class-lpc-linear-le-steps-and-parameters.php';

    $steps_and_parameters = new \LPC_linear_le_steps_and_parameters();
    $this->steps = $steps_and_parameters->steps;
    $this->parameters = $steps_and_parameters->parameters;
  }

  public function includes(){
    
    include_once LPC_LINEAR_LE_DIR . 'includes/class-lpc-linear-le-spec-display.php';
    include_once LPC_LINEAR_LE_DIR . 'templates/lpc-linear-le-view-navigator.php';
    include_once LPC_LINEAR_LE_DIR . 'includes/class-lpc-linear-le-products.php';

    include_once LPC_LINEAR_LE_DIR . 'includes/class-lpc-linear-le-enqueue-scripts.php';
     
    include_once LPC_LINEAR_LE_DIR . 'includes/class-lpc-linear-le-calculations.php';    
    include_once LPC_LINEAR_LE_DIR . 'includes/class-lpc-linear-le-spec-display.php';
    include_once LPC_LINEAR_LE_DIR . 'includes/class-lpc-linear-le-charts.php';
    include_once LPC_LINEAR_LE_DIR . 'includes/class-lpc-linear-le-validator.php';

    //templates
    include_once LPC_LINEAR_LE_DIR . 'templates/parts/lpc-linear-le-product-spec-container.php';
    include_once LPC_LINEAR_LE_DIR . 'templates/parts/lpc-linear-le-product-diagram-container.php';
    include_once LPC_LINEAR_LE_DIR . 'templates/parts/lpc-linear-le-leadscrew-modifications-container.php';
    include_once LPC_LINEAR_LE_DIR . 'templates/parts/lpc-linear-le-nut-options-container.php';

    include_once LPC_LINEAR_LE_DIR . 'includes/class-lpc-linear-le-price-leadtime.php';  
    include_once LPC_LINEAR_LE_DIR . 'templates/steps/lpc-linear-le-template-model.php';
    include_once LPC_LINEAR_LE_DIR . 'templates/steps/lpc-linear-le-step-motor-series-template.php';
    include_once LPC_LINEAR_LE_DIR . 'templates/steps/lpc-linear-le-step-motor-length-template.php';
    include_once LPC_LINEAR_LE_DIR . 'templates/steps/lpc-linear-le-step-leadscrew-options-template.php';
    include_once LPC_LINEAR_LE_DIR . 'templates/steps/lpc-linear-le-step-leadscrew-modifications-template.php';
    include_once LPC_LINEAR_LE_DIR . 'templates/steps/lpc-linear-le-step-nut-options-template.php';
    include_once LPC_LINEAR_LE_DIR . 'templates/steps/lpc-linear-le-step-review-template.php';
    
    include_once LPC_LINEAR_LE_DIR . 'templates/lpc-linear-le-templates.php';

    //ajax events
    include_once LPC_LINEAR_LE_DIR . 'includes/class-lpc-linear-le-ajax-events.php';
  }

  public function completed_steps(){
    $validator = new \LPC_linear_le_validator;
    return $validator->completed_steps();
  }

  /**
   * Generate and set product data in session
   *
   * @return void
   */
  public function set_products_data_session(){
    $products = new \LPC_linear_le_products;
    $products->set_products_data_session();
  }

  /**
   * Generate and set chart data in session
   *
   * @return void
   */
  public function set_chart_data_session(){
    $charts = new \Lpc_linear_le_charts;
    $charts->set_chart_data_session();
  }

  /**
   * Get chart data from session
   *
   * @return void
   */
  public function get_chart_data_session(){
    if(isset($_SESSION['linear_le_chart_data'])){
      return $_SESSION['linear_le_chart_data'];
    } else {
      return null;
    }
  }

  /**
   * Get price
   *
   * @return void
   */
  public function get_price(){
    $class = new \LPC_linear_le_price_leadtime;
    return $class->get_price();
  }


  /**
   * Get lead time
   *
   * @return void
   */
  public function get_lead_time(){
    $class = new \LPC_linear_le_price_leadtime;
    $class->get_lead_time();
  }
 

  /**
   * Enqueued register scripts when this configurator is loaded
   */
  public function enqueue_scripts(){
    if(param(CATEGORY) == 'linear' && param(TYPE) == 'le'){
      $scripts = new \LPC_linear_le_enqueue_scripts;
      $scripts->enqueue_scripts();
    }
  }
}

/**
 * Register ajax events on initial wp load
 */
LPC_configurator_setup::register_ajax_events();
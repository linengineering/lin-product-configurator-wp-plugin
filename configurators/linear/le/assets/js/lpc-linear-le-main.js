/**
 * Global Constants
 */

var lpc_linear_le = new function(){

    this.FORCE         = 'i1';
    this.SPEED         = 'i2';
    this.VOLTAGE       = 'i3';
    this.AMPERAGE      = 'i4';
    this.SERIES        = 'p1';
    this.MOTOR         = 'p2';
    this.SCREW         = 'p3';
    this.SCREW_LENGTH  = 'p4';
    this.SCREW_COATING = 'p5';
    this.SCREW_FINISH  = 'p6';
    this.NUT           = 'p7';
    this.COLOR_SERIES  = 'p8';
    this.COLOR_MOTOR   = 'p9';
    this.COLOR_SCREW   = 'p10';

    /** Main Content AJAX call */
    this.get_main_content = function(){
        lpc.show_spinner('.main-area');
        lpc_ajax.ajax_post('get_main').done(function(response){
            $('#main-content').html(response).foundation();
            this.load_chart_data();
            lpc.hide_spinner('.main-area');

            $('.slider').foundation();
            lpc.reinit_equalizer();

        }.bind(this))
    }


    /** Sidebar AJAX call */
    this.get_sidebar_content = function(){
        lpc.show_spinner('.side-area');
        lpc_ajax.ajax_post('get_side').done(function(response){
            $('#sidebar-content').html(response).foundation();
            this.get_price();
            this.get_lead_time();
            lpc.hide_spinner('.side-area');
        }.bind(this));
    }
    
    /** Sidebar AJAX call */
    this.get_navbar_content = function(){
        lpc_ajax.ajax_post('get_nav').done(function(response){
            $('#navbar-content').fadeIn().html(response).foundation();
            lpc.reinit_sticky();
        });
    }

    /** Get Elements AJAX call */
    this.get_elements = function(){

        lpc.show_spinner('#motor_dimensions, #motor_specifications, #leadscrew-modifications, #nut-modifications');
        $('.total-price-value, .total-lead-time-value').html('<i class="fas fa-spinner fa-spin"></i>');

        lpc_ajax.ajax_post('get_elements').done(function(response){
            this.update_price(response['element_price']);
            this.update_leadtime(response['element_leadtime']);
            this.update_element('#motor_dimensions', response['element_motor_dimensions']);
            this.update_element('#motor_specifications', response['element_motor_specifications']);
            this.update_element('#leadscrew-modifications', response['element_end_finish_dimensions']);
            this.update_element('#nut-modifications', response['element_nut_dimensions']);
            this.update_element('#navbar-content', response['element_nav']);

            lpc.reinit_sticky();
            lpc.reinit_equalizer();

        }.bind(this));
    }

    this.update_element = function(name, data){
        if($(name).length > 0) {
            $(name).html(data).foundation();
            lpc.hide_spinner(name);
        }
    }

    this.update_price = function(data){
        var price = parseFloat(data).toFixed(2);
        if(price == 0){
            price = 'TBD';
        }
        $('.total-price-value').html('$' + price);
    }

    this.update_leadtime = function(data){
        var lead = parseFloat(data);
        if(lead == 0){
            lead = 'TBD';
        }
        $('.total-lead-time-value').html( lead + ' Days');
    }

    this.get_price = function(){
        $('.total-price-value').html('<i class="fas fa-spinner fa-spin"></i>');
        lpc_ajax.ajax_post('get_price').done(function(response){
            var price = parseFloat(response).toFixed(2);
            if(price == 0){
                price = 'TBD';
            }
            $('.total-price-value').html('$' + price);
        });
    }

    this.get_lead_time = function(){
        $('.total-lead-time-value').html('<i class="fas fa-spinner fa-spin"></i>');
        lpc_ajax.ajax_post('get_lead_time').done(function(response){
            var lead = parseFloat(response);
            if(lead == 0){
                lead = 'TBD';
            }
            $('.total-lead-time-value').html( lead + ' Days');
        });
    }

    this.load_chart_data = function(){
        lpc_chart.set_chart();
        lpc_linear_le_chart.load();
    }


    this.get_all_elements = function(){
        this.get_navbar_content();
        this.get_main_content();
        this.get_sidebar_content();
    }

    this.navigate = function(data){
        var step_data    = $(data).data('step');
        var substep_data = $(data).data('substep');
        lpc.set_query_string({ [lpc.STEP]: step_data, [lpc.SUBSTEP]: substep_data })
        this.get_all_elements();
    }

    this.select = function(property, name){
        this.clear_select(name);
        $(property).css('border-top-color', property.dataset['color']);
        $(property).find('.selected-check').addClass( "selected-check-show");
        $(property).addClass( "active" );
    }

    this.clear_select = function(name){

        $(name).each(function(){
            $(name).removeClass("active noClass");
            $('.selected-check').removeClass("selected-check-show noClass");
            if(this.dataset['willWork']){
                $(this).css('border-top-color', this.dataset['color']);
            } else {
                $(this).css('border-top-color', '#AAAAAA');
            }
        });

    }

} // END OF LINEAR LE CLASS


/**
 * step navigation
 */
lpc.click('.step, .substep', function(){
    if($(this).hasClass('complete')){
        $('.substep').removeClass('active noClass');
        $(this).addClass('active');
        lpc_linear_le.navigate(this);
    }
});

lpc.click('.step-button', function(){
    if($(this).hasClass('enabled')){
        lpc.show_spinner('.main-area');
        lpc.show_spinner('.side-area');
        lpc_linear_le.navigate(this);
    }
});  

/**
 * Chart Recalculation
 */
lpc.click('#chart_calc_button', function(){

    lpc.show_spinner('.main-area, .side-area');

    var force    = $('input[name="force"]').val();
    var speed    = $('input[name="speed"]').val();
    var voltage  = $('input[name="voltage"]').val();
    var amperage = $('input[name="amperage"]').val();

    lpc.set_query_string({ 
        [lpc_linear_le.FORCE]: force, 
        [lpc_linear_le.SPEED]: speed, 
        [lpc_linear_le.VOLTAGE]: voltage, 
        [lpc_linear_le.AMPERAGE]: amperage,
    });

    lpc_ajax.configurator_setup();
});

/**
 * 
 * MOTOR / SERIES EVENTS
 * 
 */
lpc.click('.series-select', function(){

    var id = parseInt(this.id);

    //var color = lpc_ajax.chart_data['series'][id]['color'];

    lpc_linear_le.select(this, '.series-select');
    //$(this).css('border-top-color', color);

    lpc.set_query_string({ [lpc_linear_le.SERIES]: id });
    lpc.reset([
        lpc_linear_le.MOTOR,
        lpc_linear_le.SCREW,
        lpc_linear_le.SCREW_LENGTH,
        lpc_linear_le.SCREW_COATING,
        lpc_linear_le.SCREW_FINISH,
        lpc_linear_le.NUT,
    ]);
    lpc_linear_le_chart.series_chart();
    lpc_linear_le.get_elements();

});

lpc.click('.clear-series-selection', function(){

    lpc_linear_le.clear_select('.series-select');
    lpc.reset([
        lpc_linear_le.SERIES,
        lpc_linear_le.MOTOR,
        lpc_linear_le.SCREW,
        lpc_linear_le.SCREW_LENGTH,
        lpc_linear_le.SCREW_COATING,
        lpc_linear_le.SCREW_FINISH,
        lpc_linear_le.NUT,
    ]);
    lpc_linear_le_chart.series_chart();
    lpc_linear_le.get_elements();

});


/**
 * 
 * MOTOR / LENGTH EVENTS
 * 
 */
lpc.click('.motor-select', function(){

    var id = parseInt(this.id);
    lpc_linear_le.select(this, '.motor-select');
    lpc.set_query_string({ [lpc_linear_le.MOTOR]: id });
    lpc.reset([
        lpc_linear_le.SCREW,
        lpc_linear_le.SCREW_LENGTH,
        lpc_linear_le.SCREW_COATING,
        lpc_linear_le.SCREW_FINISH,
        lpc_linear_le.NUT,
    ]);
    lpc_linear_le_chart.motor_chart();
    lpc_linear_le.get_elements();

});

lpc.click('.clear-motor-selection', function(){

    lpc_linear_le.clear_select('.motor-select');
    lpc.reset([
        lpc_linear_le.MOTOR,
        lpc_linear_le.SCREW,
        lpc_linear_le.SCREW_LENGTH,
        lpc_linear_le.SCREW_COATING,
        lpc_linear_le.SCREW_FINISH,
        lpc_linear_le.NUT,
    ]);
    lpc_linear_le_chart.motor_chart();
    lpc_linear_le.get_elements();

});


/**
 * 
 * SCREW / OPTIONS EVENTS
 * 
 */
lpc.click('.screw-select', function(){

    var id = parseInt(this.id);
    lpc_linear_le.select(this, '.screw-select');
    lpc.set_query_string({ [lpc_linear_le.SCREW]: id, [lpc_linear_le.SCREW_LENGTH]: 300 });
    lpc.reset([
        lpc_linear_le.SCREW_COATING,
        lpc_linear_le.SCREW_FINISH,
        lpc_linear_le.NUT,
    ]);
    lpc_linear_le_chart.screw_chart();
    lpc_linear_le.get_elements();

});

lpc.click('.clear-screw-selection', function(){

    lpc_linear_le.clear_select('.screw-select');
    lpc.reset([
        lpc_linear_le.SCREW,
        lpc_linear_le.SCREW_LENGTH,
        lpc_linear_le.SCREW_COATING,
        lpc_linear_le.SCREW_FINISH,
        lpc_linear_le.NUT,
    ]);
    lpc_linear_le_chart.screw_chart();
    lpc_linear_le.get_elements();

});

/**
 * 
 * SCREW / MODIFICATION EVENTS
 * 
 */
lpc.click('.end-finish-select', function(){

    var id = parseInt(this.id);
    lpc_linear_le.select(this, '.end-finish-select');
    lpc.set_query_string({ [lpc_linear_le.SCREW_FINISH]: id });
    lpc_linear_le_chart.single_screw_chart();
    lpc_linear_le.get_elements();

});

lpc.click('#enter_screw_length', function(){

    var len = $('#screw_length').val();
    lpc.set_query_string({ [lpc_linear_le.SCREW_LENGTH]: len });
    lpc_linear_le.get_elements();

});


/**
 * 
 * NUT / OPTIONS EVENTS
 * 
 */
lpc.click('.nut-select', function(){

    var id = parseInt(this.id);
    lpc_linear_le.select(this, '.nut-select');
    lpc.set_query_string({ [lpc_linear_le.NUT]: id });
    lpc_linear_le_chart.single_screw_chart();
    lpc_linear_le.get_elements();

});





/**
 * 
 * 
 * Configuration Page
 * 
 * 
 * 
 */

$(document).on('change', 'input[name="quantity"]', function(){
    var qty = $('input[name="quantity"]').val();
    if(qty > 5){
        $('#message-box').foundation('open');
        $('#message-content').html('<p>To ensure fast prototype turnaround, order quantity is limited to 5 prototypes of the same configuration. For higher quantity orders, please email salesteam@linengineering.com.</p><button class="button" data-close aria-label="Close modal">Got It</button>');
        $('input[name="quantity"]').val(5);
    }
});

lpc.click('#add-to-cart', function(){
    $('#message-box').foundation('open');
    $('#message-content').html('<p><i class="fas fa-spinner fa-spin"></i> Processing, please wait...</p>');
    var qty = $('input[name="quantity"]').val();
    lpc_ajax.ajax_post('add_to_cart', {'order_qty': qty}).done(function(response){
        $('#message-content').html(response);
    });
});

lpc.click('#edit_motor', function(){
    lpc.set_query_string({ 
        [lpc.STEP]: 'motor',  
        [lpc.SUBSTEP]: 'series',  
    });
    lpc_linear_le.get_all_elements();
});

lpc.click('#edit_screw', function(){
    lpc.set_query_string({ 
        [lpc.STEP]: 'leadscrew',  
        [lpc.SUBSTEP]: 'options',  
    });
    lpc_linear_le.get_all_elements();
});

lpc.click('#edit_screw_length, #edit_screw_end_finish', function(){
    lpc.set_query_string({ 
        [lpc.STEP]: 'leadscrew',  
        [lpc.SUBSTEP]: 'modifications',  
    });
    lpc_linear_le.get_all_elements();
});

lpc.click('#edit_nut', function(){
    lpc.set_query_string({ 
        [lpc.STEP]: 'nut',  
        [lpc.SUBSTEP]: 'options',  
    });
    lpc_linear_le.get_all_elements();
});

var lpc_linear_le_chart = new function(){

    /**
     * update title, subtitle, load datasets
     */
    this.load = function(){

        var step    = lpc.param(lpc.STEP); 
        var substep = lpc.param(lpc.SUBSTEP); 
        var series  = lpc.param(lpc_linear_le.SERIES); 
        var motor   = lpc.param(lpc_linear_le.MOTOR); 
        var screw   = lpc.param(lpc_linear_le.SCREW);     

        if(step == 'motor' && substep == 'series'){
            this.series_chart();
        } else if (step == 'motor' && substep == 'length') {
            this.motor_chart();
        } else if (step == 'leadscrew' && substep == 'options') {
            this.screw_chart();
        } else {
            this.single_screw_chart();
        }

    }

    /**
     * CanvasJS datasets settings
     * @param {array} datapoints 
     * @param {string} type 
     * @param {string} color 
     * @param {int} lineThickness 
     * @param {boolean} tooltip 
     * @param {boolean} highlighEnable 
     */
    this.dataset = function(datapoints, type = 'spline', color = '#DDDDDD', lineThickness = 2, tooltip = null, highlighEnable = false){
        var data = {
            "type"             : type,
            "dataPoints"       : datapoints,
            "lineThickness"    : lineThickness,
            "markerSize"       : 1,
            "toolTipContent"   : tooltip, 
            "highlightEnabled" : highlighEnable,
            "fillOpacity"      : .1,
            "color"            : color,
            "lineColor"        : color,
        };
        return data;
    }

    /**
     * Display selected force at speed. Force = y coordinate, speed = x coordinate. 
     */
    this.force_speed_datapoint = function(){

        var force = parseFloat(lpc.param(lpc_linear_le.FORCE));
        var speed = parseFloat(lpc.param(lpc_linear_le.SPEED));

        if(speed > 0 && force > 0){
            var datapoint = {
                "type"               : "spline",
                "markerSize"         : 12,
                "dataPoints"         : [{"x": speed, "y": force}],
                "color"              : 'red',
                "xValueFormatString" : "Selected Force/Speed: #,## in/sec",
                "yValueFormatString" : "#,## lb",
            };
            return datapoint; 
        } else {
            return null;
        }
    }

    /**
     * Chart settings for series level charts  
     */
    this.series_chart = function(){

        var selected_series  = lpc.param(lpc_linear_le.SERIES); 

        var chart_data       = lpc_ajax.chart_data;
        var title            = chart_data['title'];
        var subtitle         = chart_data['subtitle'];
        var datasets         = [];

        $.each(chart_data['series'], function(key, value){
            if(selected_series == ''){
                if( value['force']['will_work'] ){
                    datasets.push(this.dataset( value['datapoints'], 'line', value['color'] )); 
                } else {
                   datasets.push(this.dataset( value['datapoints'], 'line' )); 
                }
            } else {
                
                if(key == selected_series){
                    datasets.push(this.dataset( value['datapoints'], 'area', value['color'] ));
                    title    = value['title'];
                    subtitle = value['subtitle'];
                } else {
                    datasets.push(this.dataset( value['datapoints'], 'line' ));
                }
            }
        }.bind(this));

        var force_speed = this.force_speed_datapoint();
        if(force_speed != null){
            datasets.push(force_speed);
        }

        lpc_chart.replace_datasets(datasets);
        lpc_chart.replace_title(title);
        lpc_chart.replace_subtitle(subtitle);
    }

    /**
     * Chart settings for motor level charts  
     */
    this.motor_chart = function(){
        var selected_series  = lpc.param(lpc_linear_le.SERIES); 
        var selected_motor   = lpc.param(lpc_linear_le.MOTOR); 

        var chart_data       = lpc_ajax.chart_data['series'][selected_series];
        var title            = chart_data['title'];
        var subtitle         = chart_data['subtitle'];
        var datasets         = [];

        $.each(chart_data['motors'], function(key, value){
            if(selected_motor == ''){
                if( value['force']['will_work'] ){
                    datasets.push(this.dataset( value['datapoints'], 'line', value['color'] )); 
                } else {
                   datasets.push(this.dataset( value['datapoints'], 'line' )); 
                }
                
            } else {
                if(key == selected_motor){
                    datasets.push(this.dataset( value['datapoints'], 'area', value['color'] ));
                    title = value['title'];
                    subtitle = value['subtitle'];
                } else {
                    datasets.push(this.dataset( value['datapoints'], 'line' ));
                }
            }
        }.bind(this));

        var force_speed = this.force_speed_datapoint();
        if(force_speed != null){
            datasets.push(force_speed);
        }

        lpc_chart.replace_datasets(datasets);
        lpc_chart.replace_title(title);
        lpc_chart.replace_subtitle(subtitle);
    }

    /**
     * Chart settings for leadscrew level charts  
     */
    this.screw_chart = function(){
        var selected_series  = lpc.param(lpc_linear_le.SERIES); 
        var selected_motor   = lpc.param(lpc_linear_le.MOTOR); 
        var selected_screw   = lpc.param(lpc_linear_le.SCREW);   

        var chart_data       = lpc_ajax.chart_data['series'][selected_series]['motors'][selected_motor];
        var title            = chart_data['title'];
        var subtitle         = chart_data['subtitle'];
        var datasets         = [];

        $.each(chart_data['screws'], function(key, value){
            if(selected_screw == ''){
                if( value['force']['will_work'] ){
                    datasets.push(this.dataset( value['datapoints'], 'spline', value['color'] )); 
                } else {
                   datasets.push(this.dataset( value['datapoints'], 'spline' )); 
                }
            } else {
                if(key == selected_screw){
                    datasets.push(this.dataset( value['datapoints'], 'splineArea', value['color'] ));
                    title = value['title'];
                    subtitle = value['subtitle'];
                } else {
                    datasets.push(this.dataset( value['datapoints'], 'spline' ));
                }
            }
        }.bind(this));

        var force_speed = this.force_speed_datapoint();
        if(force_speed != null){
            datasets.push(force_speed);
        }

        lpc_chart.replace_datasets(datasets);
        lpc_chart.replace_title(title);
        lpc_chart.replace_subtitle(subtitle);
    }

    /**
     * display single leadscrew
     */
    this.single_screw_chart = function(){

        var selected_series  = lpc.param(lpc_linear_le.SERIES); 
        var selected_motor   = lpc.param(lpc_linear_le.MOTOR); 
        var selected_screw   = lpc.param(lpc_linear_le.SCREW);   

        var chart_data       = lpc_ajax.chart_data['series'][selected_series]['motors'][selected_motor]['screws'][selected_screw];
        var title            = chart_data['title'];
        var subtitle         = chart_data['subtitle'];
        var datasets         = [];

        datasets.push(this.dataset( chart_data['datapoints'], 'splineArea', chart_data['color'] ));

        var force_speed = this.force_speed_datapoint();
        if(force_speed != null){
            datasets.push(force_speed);
        }

        lpc_chart.replace_datasets(datasets);
        lpc_chart.replace_title(title);
        lpc_chart.replace_subtitle(subtitle);

    }

} // END OF CLASS